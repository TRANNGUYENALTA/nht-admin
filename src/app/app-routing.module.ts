import {NgModule} from '@angular/core';

import {Routes, RouterModule, ExtraOptions} from '@angular/router';
import {AppPreloadingStrategy} from './custom-preloading';
const routes: Routes = [
  {
    path: 'pages',
    loadChildren: './modules/home/home.module#HomeModule',
  },
  {
    path: 'auth',
    loadChildren: './modules/auth/auth.module#AuthModule'
  },

  {path: '', redirectTo: 'pages', pathMatch: 'full'},
  {path: '**', redirectTo: 'auth'},
];

const config: ExtraOptions = {
  useHash: true,
  preloadingStrategy: AppPreloadingStrategy
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: [AppPreloadingStrategy] // support preloading in angular
})
export class AppRoutingModule {
}
