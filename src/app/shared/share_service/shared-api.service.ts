import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {BaseUrlService} from '../base_url/base-url.service';
import {catchError, map} from 'rxjs/operators';
import swal from 'sweetalert';


@Injectable({
  providedIn: 'root'
})
export class SharedApiService {
  DEFAULT_ERROR_TITLE = 'Something went wrong';

  constructor(private http: HttpClient,
              private baseUrlService: BaseUrlService,
              private router: Router) {
  }
  // Get method
  getApi(url: string): Observable<any> {
    const _url = `${this.baseUrlService.getBaseUrl()}` + url;
    return this.http.get<any>(_url)
      .pipe(map((response) => {
        return response;
      }),
        catchError((error: HttpErrorResponse) => {
        return this.handleErrorResponse(error);
      })
      );
  }

  // Post method
  postApi(url: string, data): Observable<any> {
    return this.http.post<any>(`${this.baseUrlService.getBaseUrl()}` + url, data)
      .pipe(map((response) => {
        return response;
      }), catchError((error: HttpErrorResponse) => {
        return this.handleErrorResponse(error);
      }));
  }

  // Put Method
  putApi(url, data): Observable<any> {
    return this.http.put<any>(`${this.baseUrlService.getBaseUrl()}` + url, data)
      .pipe(map((response) => {
        return response;
      }), catchError((error: HttpErrorResponse) => {
        return this.handleErrorResponse(error);
      }));
  }

  // Delete Method
  deleteApi(url, id): Observable<any> {
    return this.http.delete<any>(`${this.baseUrlService.getBaseUrl()}` + `${url}/${id}`)
      .pipe(map((response) => {
        return response;
      }), catchError((error: HttpErrorResponse) => {
        return this.handleErrorResponse(error);
      }));
  }

  handleErrorResponse(error: HttpErrorResponse) {

    switch (error.status) {
      case 400:
        swal({
          title: 'Something went wrong!',
          text: error.error.message,
          icon: 'error',
        });
        return throwError(new Error(error.error.message));
      case 401:
        alert(1)
        this.router.navigateByUrl('/pages/register');
        swal({
          title: 'Something went wrong!',
          text: error.error.message,
          icon: 'error',
        });
        return throwError(new Error(error.error.message));
      case 403:
        swal({
          title: 'Something went wrong!',
          text: error.error.message,
          icon: 'error',
        });
        this.router.navigateByUrl('/unauthorized');
        return throwError(new Error(error.error.message));
      default:
        swal({
          title: 'Something went wrong!',
          text: error.error.message,
          icon: 'error',
        });
        return throwError(new Error(this.DEFAULT_ERROR_TITLE));
    }
  }
}
