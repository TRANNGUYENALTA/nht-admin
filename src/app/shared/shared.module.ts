import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {HttpClientModule} from '@angular/common/http';
// import pipes
import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  FilterPipe
} from './pipes';
import {HightLightDirective} from './directives/hightLight/hight-light.directive';
import {UnlessDirective} from './directives/unless/unless.directive';
// import component
import {
  BarchartComponent,
  LineChartComponent,
  DoughnutChartComponent,
  PiechartComponent,
  PolarComponent,
  WeekysaleComponent,
  SmartTableComponent,
  RegisterComponent,
  SignIn1Component
} from './components';
const BASE_MODULES = [
  CommonModule,
  HttpClientModule,
  FormsModule,
  RouterModule,
  Ng2SmartTableModule];
const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  FilterPipe
];
const DIRECTIVES = [HightLightDirective, UnlessDirective];
const COMPONENTS = [
  SmartTableComponent,
  LineChartComponent,
  BarchartComponent,
  DoughnutChartComponent,
  PolarComponent,
  PiechartComponent,
  WeekysaleComponent,
  SignIn1Component,
  RegisterComponent
];

@NgModule({
  imports: [
    ...BASE_MODULES
  ],
  exports: [...PIPES, ...BASE_MODULES, ...DIRECTIVES, ...COMPONENTS],
  declarations: [...PIPES, ...DIRECTIVES, ...COMPONENTS]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
