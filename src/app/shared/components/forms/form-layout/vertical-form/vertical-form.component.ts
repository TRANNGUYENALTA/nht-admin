import {Component, OnInit} from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-vertical-form',
  templateUrl: './vertical-form.component.html',
  styleUrls: ['./vertical-form.component.css']
})
export class VerticalFormComponent implements OnInit {
  public infor: Infor = new Infor();

  constructor() {
  }

  ngOnInit() {
  }

  onSignIn(f) {
    console.log(f.value);
  }
}

