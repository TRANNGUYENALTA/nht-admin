import { Component, OnInit } from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-horizontal-form',
  templateUrl: './horizontal-form.component.html',
  styleUrls: ['./horizontal-form.component.css']
})
export class HorizontalFormComponent implements OnInit {
  public infor: Infor = new Infor();
  constructor() { }

  ngOnInit() {
  }
  onSignIn(f) {
    console.log(f.value);
  }
}
