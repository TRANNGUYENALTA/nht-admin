export class Infor {
  public name: string;
  public email: string;
  public mobile: number;
  public password: string;
  public repassword: string;
  public remember: any;

  constructor() {
    this.name = '';
    this.email = '';
    this.mobile = null;
    this.password = '';
    this.repassword = '';
    this.remember = true;
  }
}

