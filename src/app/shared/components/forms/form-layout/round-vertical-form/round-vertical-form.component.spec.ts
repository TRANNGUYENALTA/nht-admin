import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundVerticalFormComponent } from './round-vertical-form.component';

describe('RoundVerticalFormComponent', () => {
  let component: RoundVerticalFormComponent;
  let fixture: ComponentFixture<RoundVerticalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundVerticalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundVerticalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
