import {Component, OnInit} from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-round-vertical-form',
  templateUrl: './round-vertical-form.component.html',
  styleUrls: ['./round-vertical-form.component.css']
})
export class RoundVerticalFormComponent implements OnInit {
  public infor: Infor = new Infor();
  constructor() {
  }

  ngOnInit() {
  }

  onSignIn(f) {
    console.log(f.value);
  }
}
