import {Component, OnInit} from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-round-horizontal-form',
  templateUrl: './round-horizontal-form.component.html',
  styleUrls: ['./round-horizontal-form.component.css']
})
export class RoundHorizontalFormComponent implements OnInit {
  public infor: Infor = new Infor();

  constructor() {
  }

  ngOnInit() {
  }

  onSignIn(f) {
    console.log(f.value);
  }
}
