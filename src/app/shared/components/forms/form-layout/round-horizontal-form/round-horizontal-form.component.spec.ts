import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundHorizontalFormComponent } from './round-horizontal-form.component';

describe('RoundHorizontalFormComponent', () => {
  let component: RoundHorizontalFormComponent;
  let fixture: ComponentFixture<RoundHorizontalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundHorizontalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundHorizontalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
