import {Component, OnInit} from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-back-to-top',
  templateUrl: './back-to-top.component.html',
  styleUrls: ['./back-to-top.component.css']
})
export class BackToTopComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    /* Back To Top */
    $(document).ready(function () {
      $(window).on('scroll', function () {
        if ($(this).scrollTop() > 300) {
          $('.back-to-top').fadeIn();
        } else {
          $('.back-to-top').fadeOut();
        }
      });
      $('.back-to-top').on('click', function () {
        $('html, body').animate({scrollTop: 0}, 600);
        return false;
      });
    });
  }

}
