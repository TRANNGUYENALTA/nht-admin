export function drawChart(data) {
  getData(data).then(function (res) {
    // chart Char js type line
    var ctx = document.getElementById('site-traffic-chart').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: res,
      options: {
        legend: {
          display: true,
          labels: {
            fontColor: '#ddd',
            boxWidth: 40
          }
        },
        tooltips: {
          displayColors: true
        },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: '#ddd'
            },
            gridLines: {
              display: true,
              color: "rgba(221, 221, 221, 0.08)"
            },
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: '#ddd'
            },
            gridLines: {
              display: true,
              color: "rgba(221, 221, 221, 0.08)"
            },
          }]
        }
      }
    });
  })


}

async function getData(data) {
  // console.log('function run before!');
  var result = await convertData(data)
  return result
}

function convertData(res = []) {
  var labels = [];
  var datasets = [];
  res.forEach((value, index) => {
    if(labels.length < 1){
      labels = Object.keys(value.data)
    }
    datasets.push({
      label: value.name,
      data: Object.values(value.data),
      backgroundColor: random_rgba(),
      borderColor: "transparent",
      pointRadius: "0",
      borderWidth: 1
    })
    // console.log(index)
    // console.log(value.name)
    // console.log(Object.keys(value.data));
    // console.log(Object.values(value.data));
  })
  var data = {
    labels: labels,
    datasets:datasets
  }
  return data;
}

function random_rgba() {
  var o = Math.round, r = Math.random, s = 255;
  return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
}

// data mau
//
// {
//   labels: lable,
//     datasets: [{
//   label: oldData.title,
//   data: oldData.data,
//   backgroundColor: 'rgba(248,225,144,0.4)',
//   borderColor: "transparent",
//   pointRadius: "0",
//   borderWidth: 3
// }, {
//   label: newData.title,
//   data: newData.data,
//   backgroundColor: "rgba(6,234,99,0.3)",
//   borderColor: "transparent",
//   pointRadius: "0",
//   borderWidth: 1
// },
//   {
//     label: thirdData.title,
//     data: thirdData.data,
//     backgroundColor: "rgba(255, 155, 255, 0.25)",
//     borderColor: "transparent",
//     pointRadius: "0",
//     borderWidth: 1
//   }]
// }
