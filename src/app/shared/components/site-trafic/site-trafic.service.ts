import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SiteTraficService {

  constructor(private shareApiService: SharedApiService) {
  }

  getData(): Observable<any> {
    return this.shareApiService.getApi('/api/dashboard/ecommerce/site-traffic');
  }
}
