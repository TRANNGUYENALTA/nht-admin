import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteTraficComponent } from './site-trafic.component';

describe('SiteTraficComponent', () => {
  let component: SiteTraficComponent;
  let fixture: ComponentFixture<SiteTraficComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteTraficComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteTraficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
