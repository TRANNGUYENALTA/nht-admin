import {Component, OnInit} from '@angular/core';
import {drawChart} from './chart';
import {Subscription} from 'rxjs';
import {SiteTraficService} from './site-trafic.service';

@Component({
  selector: 'app-site-trafic',
  templateUrl: './site-trafic.component.html',
  styleUrls: ['./site-trafic.component.css']
})
export class SiteTraficComponent implements OnInit {
  private _unsubscribe = new Subscription();
  public data;
  constructor(private siteTrafficService: SiteTraficService) {
  }

  ngOnInit() {
    this.getDataSite();
  }

  getDataSite() {
    this._unsubscribe = this.siteTrafficService.getData().subscribe(res => {
      this.data = res.data;
      drawChart(this.data);
    });
  }

}
