export class UserRegister {
  public employee_code: string;
  public name: string;
  public email: string;
  public password: string;
  public repassword: string;

  constructor() {
    this.employee_code = '';
    this.name = '';
    this.email = '';
    this.password = '';
    this.repassword = '';
  }
}
