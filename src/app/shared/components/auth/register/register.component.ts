import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserRegister} from './infor';
import {AuthService} from '../services/auth.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('formRegister') _formRegister;
  public userRegiste: UserRegister = new UserRegister();
  public _subscription = new Subscription();

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  register() {
    console.log(this._formRegister.value);
    this._subscription = this.authService.register(this._formRegister.value).subscribe(res => {
      swal({
        title: 'Good Look',
        text: 'Regiser Success!',
        icon: 'success',
      });
      this.router.navigateByUrl('auth/login');
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
