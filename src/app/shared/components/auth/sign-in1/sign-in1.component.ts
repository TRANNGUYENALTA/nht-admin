import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from '@shared/components/auth/services/auth.service';
import {NgForm} from '@angular/forms';
import {User} from '@shared/components/auth/sign-in1/infor-singin1';

@Component({
  selector: 'app-sign-in1',
  templateUrl: './sign-in1.component.html',
  styleUrls: ['./sign-in1.component.css']
})
export class SignIn1Component implements OnInit, OnDestroy {

  subscription: Subscription = new Subscription();
  @ViewChild('formLogin') vlForm;
  user: User = new User();

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    localStorage.removeItem('currentUser');
  }

  onSignIn(f: NgForm) {
    // console.log(f.form.value);
    // console.log(this.vlForm.value);
    this.auth.login(this.vlForm.value);
    // this.subscription = this.auth.login(this.vlForm.value);
  }

  resetForm() {
    this.vlForm.reset();
  }

  setFormValue() {
    this.vlForm.setValue({email: 'toi.huynh@alta.com.vn', password: 'admin'});
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

}
