import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-reset-password1',
  templateUrl: './reset-password1.component.html',
  styleUrls: ['./reset-password1.component.css']
})
export class ResetPassword1Component implements OnInit {
  @ViewChild('email') emailElement;

  constructor() {
  }

  ngOnInit() {
  }

  resetPassword() {
    alert('reset password working');
    console.log(this.emailElement.nativeElement.value);
  }

}
