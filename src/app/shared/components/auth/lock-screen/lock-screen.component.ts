import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-lock-screen',
  templateUrl: './lock-screen.component.html',
  styleUrls: ['./lock-screen.component.css']
})
export class LockScreenComponent implements OnInit {
  @ViewChild('password') elementPassord;
  constructor() {
  }

  ngOnInit() {
  }

  unLock() {
    alert('request unlock screen!')
    console.log(this.elementPassord.nativeElement.value);
  }
}
