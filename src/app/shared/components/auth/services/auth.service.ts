import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../../../base_url/base-url.service'; // comment
import {Router} from '@angular/router';
import swal from 'sweetalert';
import {SharedApiService} from '@shared/share_service/shared-api.service';


@Injectable({
  providedIn: 'root'
})


export class AuthService implements OnInit {
  private token = 'Bearer ';

  constructor(private http: HttpClient,
              private baseUrl: BaseUrlService,
              private sharedApiService: SharedApiService,
              private router: Router) {
  }

  ngOnInit() {
  }

  getToken() {
    return this.token;
  }

  login(data) {
    localStorage.setItem('currentUser', 'myToken');
    this.router.navigateByUrl('pages/dashboard');
    // return this.sharedApiService.postApi(`/api/user/login`, data).subscribe(res => {
    //   if (res && res.data) {
    //     this.token = this.token + res.data.token;
    //     swal({
    //       title: 'Good Work',
    //       text: 'Login Success!',
    //       icon: 'success',
    //     });
    //     localStorage.setItem('currentUser', res.data.token);
    //   } else {
    //     localStorage.setItem('currentUser', '');
    //   }
    //   this.router.navigateByUrl('pages/dashboard');
    // }, err => {
    // });
  }

  register(data) {
    return this.sharedApiService.postApi('/api/user/register', data);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
  }
}

// Authorization': `Bearer ${AuthService.getToken()
