export class Infor {
  public userName: string;
  public passWord: string;
  public remember: any;

  constructor() {
    this.userName = '';
    this.passWord = '';
    this.remember = true;
  }
}
