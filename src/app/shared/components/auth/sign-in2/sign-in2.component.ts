import {Component, OnInit, ViewChild} from '@angular/core';
import {Infor} from './infor';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-sign-in2',
  templateUrl: './sign-in2.component.html',
  styleUrls: ['./sign-in2.component.css']
})
export class SignIn2Component implements OnInit {
  public user = new Infor()
  @ViewChild('formLogin') vlForm;
  constructor() { }

  ngOnInit() {
  }
  onSignIn(f: NgForm) {
    // console.log(f.form.value);
    console.log(this.vlForm.value);
  }

  resetForm() {
    this.vlForm.reset();
  }

  setFormValue() {
    this.vlForm.setValue({email: 'toi.huynh@alta.com.vn', password: 'admin'});
  }

}
