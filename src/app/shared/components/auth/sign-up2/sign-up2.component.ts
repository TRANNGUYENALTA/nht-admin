import {Component, OnInit} from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-sign-up2',
  templateUrl: './sign-up2.component.html',
  styleUrls: ['./sign-up2.component.css']
})
export class SignUp2Component implements OnInit {
  public infor: Infor = new Infor();

  constructor() {
  }

  ngOnInit() {
  }

  submitForm(form) {
    alert('form Sign up 2 sumitted');
    console.log(form.value);
  }

}
