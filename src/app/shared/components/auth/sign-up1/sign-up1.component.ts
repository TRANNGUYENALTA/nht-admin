import {Component, OnInit} from '@angular/core';
import {Infor} from './infor';

@Component({
  selector: 'app-sign-up1',
  templateUrl: './sign-up1.component.html',
  styleUrls: ['./sign-up1.component.css']
})
export class SignUp1Component implements OnInit {

  public infor = new Infor();
  constructor() {
  }

  ngOnInit() {
  }

  submitForm(form) {
    alert('Form SignUP-1 submitted!');
    console.log(form.value)
  }

}
