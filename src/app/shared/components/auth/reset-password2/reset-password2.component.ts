import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-reset-password2',
  templateUrl: './reset-password2.component.html',
  styleUrls: ['./reset-password2.component.css']
})
export class ResetPassword2Component implements OnInit {
  @ViewChild('emailReset') emailElement;

  constructor() {
  }

  ngOnInit() {
  }

  resetPassword() {
    alert('resetPassword working!');
    console.log(this.emailElement.nativeElement.value);
  }

}
