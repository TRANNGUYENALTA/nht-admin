import {Component, OnDestroy, OnInit} from '@angular/core';
import '../../../../assets/plugins/jquery-knob/jquery.knob.js';
import {drawChart} from './chart';
import {TotalDownloadService} from './total-download.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-total-download',
  templateUrl: './total-download.component.html',
  styleUrls: ['./total-download.component.css']
})
export class TotalDownloadComponent implements OnInit, OnDestroy {
  public data: any = {};
  private _subscription = new Subscription();

  constructor(private totalDownload: TotalDownloadService) {
  }

  ngOnInit() {
    setTimeout(function () {
      drawChart();
    });
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.totalDownload.getDatas().subscribe(res => {
      this.data = res.data;
    });
  }
}
