import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalDownloadComponent } from './total-download.component';

describe('TotalDownloadComponent', () => {
  let component: TotalDownloadComponent;
  let fixture: ComponentFixture<TotalDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
