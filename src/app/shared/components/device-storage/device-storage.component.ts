  import {Component, OnDestroy, OnInit} from '@angular/core';
  import {DeviceStorageService} from './device-storage.service';
  import {Subscription} from 'rxjs';

  @Component({
    selector: 'app-device-storage',
    templateUrl: './device-storage.component.html',
    styleUrls: ['./device-storage.component.css']
  })
  export class DeviceStorageComponent implements OnInit, OnDestroy {
    private _subscription = new Subscription();
    public data: any = {};

    constructor(private deviceStorageSevice: DeviceStorageService) {
    }

    ngOnInit() {
      this.getData();
    }

    ngOnDestroy() {
      this._subscription.unsubscribe();
    }

    getData() {
      this._subscription = this.deviceStorageSevice.getDatas().subscribe(res => {
        this.data = res.data;
      });
    }

  }
