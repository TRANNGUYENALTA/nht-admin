import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceStorageComponent } from './device-storage.component';

describe('DeviceStorageComponent', () => {
  let component: DeviceStorageComponent;
  let fixture: ComponentFixture<DeviceStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
