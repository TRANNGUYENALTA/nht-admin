import {Component, OnDestroy, OnInit} from '@angular/core';
import {drawCalendar} from './calendar';
import {FullCalendarService} from './full-calendar.service';
import {Subscription} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-full-calendar',
  templateUrl: './full-calendar.component.html',
  styleUrls: ['./full-calendar.component.css']
})
export class FullCalendarComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  private datas: Array<any> = [];

  constructor(private fullCalendarService: FullCalendarService) {
  }

  ngOnInit() {
    this.getDataCalendar();
  }
  getDataCalendar() {
    this.subscription = this.fullCalendarService.getEvent().subscribe(res => {
      this.datas = res.data;
      drawCalendar(this.datas);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  resetCalendar() {
    $('#calendar').fullCalendar('destroy');
  }


  reDraw() {
    drawCalendar(this.datas);
  }

  waitDocument(event: string, object: string, callBackFunction) {
    $(object).on(event, callBackFunction);
  }

  alertFunction = () => {
    alert(1);
  }

}
