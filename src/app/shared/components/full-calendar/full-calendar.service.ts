import {Injectable} from '@angular/core';
import {SharedApiService} from '../../share_service/shared-api.service';

@Injectable({
  providedIn: 'root'
})
export class FullCalendarService {
  constructor(private shareService: SharedApiService,
             ) {
  }

  getEvent() {
    return this.shareService.getApi('/api/calendar');
  }
}
