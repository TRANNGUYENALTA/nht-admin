var timesClick = 0;
export function drawCalendar(data) {
  console.log(data);
  $('#calendar').fullCalendar({
    // header: {
    //   left: 'prev title next',
    //   right: 'month list'
    // },
    // buttonText: {
    //   month: 'MONTH',
    //   list: 'LIST'
    // },
    header: {
      left: 'prev,next',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listMonth',
      border: 1
    },
    defaultDate: '2018-03-12',
    weekends: true,
    selectable: true, // select element by us mouse
    select: function (start, end) {
      var title = prompt('Event Title:');
      var eventData;
      if (title) {
        eventData = {
          title: title,
          start: start,
          end: end
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true=
      }
      $('#calendar').fullCalendar('unselect');
    },
    editable: true,
    // eventLimit: true, // allow "more" link when too many events

    events: data,
    eventRender: function (event, eventElement) {
      // custome view event on calendar
      eventElement.append('<div class="content_event" data-type="' + 1 + '" data-template="' + event.id + '">' + '<span class="closeon" data-id="' + event.id + '">x</span>' +
        '<div class="content"><div class="content_event_data"><span class="fc-event--title">' + event.title + '</span></div><div><span class="description_icon"></span></div></div></div>' +
        '</div>');
      eventElement.find(".closeon").click(function () {
        $('#calendar').fullCalendar('removeEvents', event._id);
      });
      eventElement.find('.description_icon').append(event.description);
    },
    eventClick: function (calEvent, jsEvent, view) {
      console.log(calEvent)
      var title = prompt('Event Title:', calEvent.title, {buttons: {Ok: true, Cancel: false}});
      if (title) {
                calEvent.title = title;
                $('#calendar').fullCalendar('updateEvent',calEvent);
              }
      // swal({
      //   title: "Are you sure?",
      //   text: "Once update, you will not be able to recover this event!",
      //   icon: "warning",
      //   buttons: true,
      //   dangerMode: true,
      // }).then((willDelete) => {
      //     if (willDelete) {
      //       if (title) {
      //         calEvent.title = title;
      //         $('#calendar').fullCalendar('updateEvent',calEvent);
      //       }
      //     }
      //   });
    },
    eventMouseover: function (event) {
      // console.log('eventMouseover', event.title);
    },
    eventMouseout: function (event) {
      // console.log('    eventMouseout', event.title);
    }
  });

   $('.fc-left').append('<button type="button" data-toggle="modal" data-target="#modal-addfeed" class="button_addfeed fc-addFeed-button fc-button fc-state-default fc-corner-left fc-corner-right " id="button_event" style="border-left: 4px solid green!important;">ADD PROMO</button>');

}

function addNewEvent(data) {
  $.ajax({
    method: "POST",
    url: "some.php",
    data: data
  }).done(function (msg) {
    alert("Data Saved: " + msg);
  });
}
