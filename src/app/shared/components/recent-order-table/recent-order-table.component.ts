import {Component, OnInit} from '@angular/core';
import {RecentOrderTableService} from './recent-order-table.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-recent-order-table',
  templateUrl: './recent-order-table.component.html',
  styleUrls: ['./recent-order-table.component.css']
})
export class RecentOrderTableComponent implements OnInit {
  private _subscription = new Subscription();
  public datas: any = [];

  constructor(private recentOrderService: RecentOrderTableService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._subscription = this.recentOrderService.getDatas().subscribe(res => {
      this.datas = res.data;
    });
  }
}

