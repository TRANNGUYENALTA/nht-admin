import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {CustomerReviewService} from './customer-review.service';

@Component({
  selector: 'app-customer-review',
  templateUrl: './customer-review.component.html',
  styleUrls: ['./customer-review.component.css']
})
export class CustomerReviewComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public data: any = [];
  public maxStar = 5;

  constructor(private customerReviewService: CustomerReviewService) {
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.customerReviewService.getDatas().subscribe(res => {
      this.data = res.data;
    });
  }

  createArrayStarLight(lenght) {
    const number = +lenght;
    return Array(number).fill(0).map((x, i) => i);
  }

  createArrayStarNotLight(lenght) {
    const number = +lenght;
    return Array(this.maxStar - number).fill(0).map((x, i) => i);
  }

}
