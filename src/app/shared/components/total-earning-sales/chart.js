export function drawChart(data1, data2) {
  var ctx = document.getElementById("chart5").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
      datasets: [{
        label: 'Total Earning',
        data: data1,
        backgroundColor: "#fff"
      },{
        label: 'Total Sales',
        data: data2,
        backgroundColor: "rgba(255, 255, 255, 0.12)"
      }]
    },
    options: {
      legend: {
        display: false,
        position: 'bottom',
        labels: {
          fontColor: '#ddd',
          boxWidth:13
        }
      },
      tooltips: {
        enabled:true,
        displayColors:false,
      },

      scales: {
        xAxes: [{
          stacked: true,
          barPercentage: .4,
          display: false,
          gridLines: false
        }],
        yAxes: [{
          stacked: true,
          display: false,
          gridLines: false
        }]
      }

    }

  });
}
