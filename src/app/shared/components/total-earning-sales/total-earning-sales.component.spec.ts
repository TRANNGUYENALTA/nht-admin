import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalEarningSalesComponent } from './total-earning-sales.component';

describe('TotalEarningSalesComponent', () => {
  let component: TotalEarningSalesComponent;
  let fixture: ComponentFixture<TotalEarningSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalEarningSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalEarningSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
