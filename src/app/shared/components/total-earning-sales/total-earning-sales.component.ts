import {Component, OnDestroy, OnInit} from '@angular/core';
import {drawChart} from './chart';
import {TotalEarningSalesService} from './total-earning-sales.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-total-earning-sales',
  templateUrl: './total-earning-sales.component.html',
  styleUrls: ['./total-earning-sales.component.css']
})
export class TotalEarningSalesComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public totalEarning: any = {};
  public totalSales: any = {};

  constructor(private totalEarningSaleService: TotalEarningSalesService) {
  }

  ngOnInit() {

    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.totalEarningSaleService.getDatas().subscribe(res => {
      this.totalEarning = res.data.total_earning;
      this.totalSales = res.data.total_sales;
      drawChart(this.totalEarning.data, this.totalSales.data);
    });
  }
}
