export function drawChart(data) {
  var ctx = document.getElementById("total-click-chart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
      datasets: [{
        label: 'Total Clicks',
        data: data,
        backgroundColor: "#fff"
      }]
    },
    options: {
      legend: {
        display: false,
        labels: {
          fontColor: '#ddd',
          boxWidth:40
        }
      },
      tooltips: {
        enabled:false
      },

      scales: {
        xAxes: [{
          barPercentage: .3,
          display: false,
          gridLines: false
        }],
        yAxes: [{
          display: false,
          gridLines: false
        }]
      }

    }

  });

}
