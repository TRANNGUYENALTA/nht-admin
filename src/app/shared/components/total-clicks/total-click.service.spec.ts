import { TestBed } from '@angular/core/testing';

import { TotalClickService } from './total-click.service';

describe('TotalClickService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TotalClickService = TestBed.get(TotalClickService);
    expect(service).toBeTruthy();
  });
});
