import {Component, OnDestroy, OnInit} from '@angular/core';
import {drawChart} from './chart';
import {TotalClickService} from './total-click.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-total-clicks',
  templateUrl: './total-clicks.component.html',
  styleUrls: ['./total-clicks.component.css']
})
export class TotalClicksComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public datas: any = {};

  constructor(private totalClickService: TotalClickService) {
  }

  ngOnInit() {
    drawChart();
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.totalClickService.getDatas().subscribe(res => {
      this.datas = res.data;
      drawChart(res.data.data);
    });
  }

}
