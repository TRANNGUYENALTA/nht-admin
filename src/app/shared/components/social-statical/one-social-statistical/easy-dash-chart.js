export function drawEasyDashChart() {
  // easy pie chart
  $('.easy-dash-chart').easyPieChart({
    easing: 'easeOutBounce',
    barColor : '#ffffff',
    lineWidth: 10,
    trackColor : 'rgba(255, 255, 255, 0.12)',
    scaleColor: false,
    onStep: function(from, to, percent) {
      $(this.el).find('.w_percent').text(Math.round(percent));
    }
  });
}
