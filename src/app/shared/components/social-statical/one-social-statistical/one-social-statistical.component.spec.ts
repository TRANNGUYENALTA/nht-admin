import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneSocialStatisticalComponent } from './one-social-statistical.component';

describe('OneSocialStatisticalComponent', () => {
  let component: OneSocialStatisticalComponent;
  let fixture: ComponentFixture<OneSocialStatisticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneSocialStatisticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneSocialStatisticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
