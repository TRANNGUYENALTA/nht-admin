import {Component, Input, OnInit} from '@angular/core';
import {drawEasyDashChart} from './easy-dash-chart';
@Component({
  selector: 'app-one-social-statistical',
  templateUrl: './one-social-statistical.component.html',
  styleUrls: ['./one-social-statistical.component.css']
})
export class OneSocialStatisticalComponent implements OnInit {
  @Input('data') data: {id: number, name: string, percentLastWeek: number, percent: number};
  public iconClass = '';
  public title = '';
  constructor() { }

  ngOnInit() {
    this.setIconClass();
    setTimeout(function () {
      drawEasyDashChart();
    });
  }

  setIconClass() {
    switch (this.data.name) {
      case 'Facebook Followers': {
        this.iconClass = 'fa fa-facebook';
        break;
      }
      case'Twitter Tweets': {
        this.iconClass = 'fa fa-twitter';
        break;
      }
      case 'Visitors': {
        this.iconClass = 'fa fa-eye';
        break;
      }
      case 'Youtube Subscribers': {
        this.iconClass = 'fa fa-youtube';
        break;
      }
      default: {
        this.iconClass = 'fa fa-shopping-cart';
        break;
      }
    }
  }
}
