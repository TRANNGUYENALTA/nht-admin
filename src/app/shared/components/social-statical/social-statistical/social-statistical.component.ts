import {Component, OnInit} from '@angular/core';
import {SocialStatisticalService} from './social-statistical.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-social-statistical',
  templateUrl: './social-statistical.component.html',
  styleUrls: ['./social-statistical.component.css']
})
export class SocialStatisticalComponent implements OnInit {
  private _subscription = new Subscription();
  public datas = []
  constructor(private socialService: SocialStatisticalService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._subscription = this.socialService.getData().subscribe(res => {
      this.datas = res.data;
    });
  }

}
