import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialStatisticalComponent } from './social-statistical.component';

describe('SocialStatisticalComponent', () => {
  let component: SocialStatisticalComponent;
  let fixture: ComponentFixture<SocialStatisticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialStatisticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialStatisticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
