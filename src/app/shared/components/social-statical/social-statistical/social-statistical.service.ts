import { Injectable } from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocialStatisticalService {

  constructor(private shareApi: SharedApiService) { }
  getData(): Observable<any> {
    return this.shareApi.getApi('/api/dashboard/ecommerce/share-ecommerce');
  }
}
