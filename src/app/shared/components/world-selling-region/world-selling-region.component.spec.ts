import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldSellingRegionComponent } from './world-selling-region.component';

describe('WorldSellingRegionComponent', () => {
  let component: WorldSellingRegionComponent;
  let fixture: ComponentFixture<WorldSellingRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldSellingRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldSellingRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
