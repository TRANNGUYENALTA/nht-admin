import {Component, OnDestroy, OnInit} from '@angular/core';
import '@assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js'; // third party implement
import '@assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js';
import {drawVectorMap, drawSparklineChart} from './vectormap';
import {WorldSellingRegionService} from './world-selling-region.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-world-selling-region',
  templateUrl: './world-selling-region.component.html',
  styleUrls: ['./world-selling-region.component.css']
})
export class WorldSellingRegionComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public datas = [];

  constructor(private worldSellingService: WorldSellingRegionService) {
  }

  ngOnInit() {
    drawVectorMap();
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.worldSellingService.getData().subscribe(res => {
      this.datas = res.data;
      const that = this;
      setTimeout(function () {
        that.drawTrendChart();
      });
    });
  }

  drawTrendChart() {
    this.datas.forEach((value, index) => {
      drawSparklineChart('#trendchart' + index, value.trend);
    });
  }
}
