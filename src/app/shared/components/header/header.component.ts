import {Component, OnInit} from '@angular/core';
import {HomeService} from '../../../modules/home/home.service';
import {Router} from '@angular/router';
import {toggleMenuAction} from './header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private homeService: HomeService,
              private router: Router) {
    toggleMenuAction();
  }

  onClickEventEmiiter() {
    this.homeService.newEventEmitter.emit('Hello Event Emitter');
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigateByUrl('/login');
  }

  ngOnInit() {
  }

}
