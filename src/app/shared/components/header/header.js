export function toggleMenuAction() {
  // === toggle-menu js
  $(document).on('click', '.toggle-menu',function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  })
}
