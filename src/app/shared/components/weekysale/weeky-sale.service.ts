import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeekySaleService {

  constructor(private sharedApi: SharedApiService) {
  }

  getData(): Observable<any> {
    return this.sharedApi.getApi('/api/dashboard/ecommerce/weekly-sales');
  }
}
