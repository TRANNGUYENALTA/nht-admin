import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekysaleComponent } from './weekysale.component';

describe('WeekysaleComponent', () => {
  let component: WeekysaleComponent;
  let fixture: ComponentFixture<WeekysaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekysaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekysaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
