import {Component, OnDestroy, OnInit} from '@angular/core';
import {drawWeekySaleChart} from './chart';
import {WeekySaleService} from './weeky-sale.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-weekysale',
  templateUrl: './weekysale.component.html',
  styleUrls: ['./weekysale.component.css']
})
export class WeekysaleComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public dataChart = [] ;
  public dataTable: Array<{id: number, name: string, price: number, percent: string}> = [];

  constructor(private weekyService: WeekySaleService) {
  }

  ngOnInit() {
    this.getData();

  }

  getData() {
    this._subscription = this.weekyService.getData().subscribe(res => {

      this.dataTable = res.data.chart1;
      this.dataChart = res.data.chart2;
      this.getArrayDataChart(this.dataChart).then(function (_res) {
        drawWeekySaleChart(_res);
      });
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  async getArrayDataChart(data) {
    const tempDataArr = [];
    const tempLabelArr = [];
    data.forEach((item, index) => {
      tempDataArr.push(item.percent);
      tempLabelArr.push(item.name);
    });
    return {
      label: tempLabelArr,
      data: tempDataArr
    };
  }

}
