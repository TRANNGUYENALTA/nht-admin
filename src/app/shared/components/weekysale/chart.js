export function drawWeekySaleChart(data) {
  if ($('#doughnutChart').length) {
    var ctx = document.getElementById("doughnutChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: data.label,
        datasets: [{
          backgroundColor: [
            "rgba(255, 255, 255, 0.35)",
            "#ffffff",
            "rgba(255, 255, 255, 0.12)",
            "rgba(255, 255, 255, 0.71)"
          ],
          data: data.data,
          borderWidth: [0, 0, 0, 0]
        }]
      },
      options: {
        legend: {
          position :"right",
          display: true,
          labels: {
            fontColor: '#ddd',
            boxWidth:15
          }
        }
      }
    });
  }

}
