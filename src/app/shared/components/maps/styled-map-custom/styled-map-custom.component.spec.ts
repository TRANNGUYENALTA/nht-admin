import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyledMapCustomComponent } from './styled-map-custom.component';

describe('StyledMapCustomComponent', () => {
  let component: StyledMapCustomComponent;
  let fixture: ComponentFixture<StyledMapCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyledMapCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyledMapCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
