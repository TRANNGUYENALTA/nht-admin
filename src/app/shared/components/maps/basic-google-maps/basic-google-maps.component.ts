import {Component, OnDestroy, OnInit} from '@angular/core';
import {BasicGoogleMapService} from './basic-google-map.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-basic-google-maps',
  templateUrl: './basic-google-maps.component.html',
  styleUrls: ['./basic-google-maps.component.css']
})
export class BasicGoogleMapsComponent implements OnInit, OnDestroy {
  private _subscription: Subscription = new Subscription();
  lat: number = 13.923897;
  lng: number = 109.018797;

  constructor(private googleMapsService: BasicGoogleMapService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._subscription = this.googleMapsService.getDatas().subscribe(res => {
      console.log(res);
      // this.lat = res.data.lat;
      // this.lng = res.data.lng;
      // console.log(this.lat, this.lng);
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
