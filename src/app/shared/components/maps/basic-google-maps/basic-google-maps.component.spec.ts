import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicGoogleMapsComponent } from './basic-google-maps.component';

describe('BasicGoogleMapsComponent', () => {
  let component: BasicGoogleMapsComponent;
  let fixture: ComponentFixture<BasicGoogleMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicGoogleMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicGoogleMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
