import {Component, OnInit} from '@angular/core';
import {LatLngLiteral} from '@agm/core';

@Component({
  selector: 'app-polygonal-map',
  templateUrl: './polygonal-map.component.html',
  styleUrls: ['./polygonal-map.component.css']
})
export class PolygonalMapComponent implements OnInit {
  constructor() {
  }

  lat = 25.774;
  lng = -80.190;
  paths: Array<LatLngLiteral> = [
    {lat: 25.774, lng: -80.190},
    {lat: 18.466, lng: -66.118},
    {lat: 32.321, lng: -64.757},
    {lat: 25.774, lng: -80.190}
  ];
  // Nesting paths will create a hole where they overlap;


  ngOnInit() {
  }

}
