import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolygonalMapComponent } from './polygonal-map.component';

describe('PolygonalMapComponent', () => {
  let component: PolygonalMapComponent;
  let fixture: ComponentFixture<PolygonalMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolygonalMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolygonalMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
