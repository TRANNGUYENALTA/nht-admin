import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-over-layer-map',
  templateUrl: './over-layer-map.component.html',
  styleUrls: ['./over-layer-map.component.css']
})
export class OverLayerMapComponent implements OnInit {
  private _subscription = new Subscription();
  url: string = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/talkeetna.png';
  lat: number = 13.923897;
  lng: number = 109.018797;

  constructor() {
    console.log('overlay map does not work');
  }

  ngOnInit() {
  }


}
