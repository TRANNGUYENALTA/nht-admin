import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverLayerMapComponent } from './over-layer-map.component';

describe('OverLayerMapComponent', () => {
  let component: OverLayerMapComponent;
  let fixture: ComponentFixture<OverLayerMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverLayerMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverLayerMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
