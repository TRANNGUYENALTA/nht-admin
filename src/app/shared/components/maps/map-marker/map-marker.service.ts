import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {SharedApiService} from '../../../share_service/shared-api.service';

@Injectable({
  providedIn: 'root'
})
export class MapMarkerService {

  constructor(private shareApi: SharedApiService) { }
  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/map/marker');
  }
}
