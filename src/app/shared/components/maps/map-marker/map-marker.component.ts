import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {MapMarkerService} from './map-marker.service';

@Component({
  selector: 'app-map-marker',
  templateUrl: './map-marker.component.html',
  styleUrls: ['./map-marker.component.css']
})
export class MapMarkerComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  lat: number = 13.923897;
  lng : number = 109.018797;

  constructor(private mapMakerService: MapMarkerService) {
  }

  ngOnInit() {
    this.getData();
  }

  markerClick(event) {
    console.log(event);
  }

  getData() {
    this._subscription = this.mapMakerService.getDatas().subscribe(res => {
      console.log(res);
      // this.lat = res.data.lat;
      // this.lng = res.data.lng;
      // console.log(this.lat, this.lng);
    });
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
