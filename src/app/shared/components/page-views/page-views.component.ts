import {Component, OnDestroy, OnInit} from '@angular/core';
import {drawChart} from './chart';
import {PageViewService} from './page-view.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-page-views',
  templateUrl: './page-views.component.html',
  styleUrls: ['./page-views.component.css']
})
export class PageViewsComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public data: any = {};

  constructor(private pageViewService: PageViewService) {
  }

  ngOnInit() {

    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.pageViewService.getDatas().subscribe(res => {
      this.data = res.data;
      drawChart(res.data.data);
    });
  }

}
