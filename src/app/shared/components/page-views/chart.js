export function drawChart(data) {
  var ctx = document.getElementById('page-view-chart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
      datasets: [{
        label: 'Page Views',
        data: data,
        backgroundColor: 'rgba(255, 255, 255, 0.12)',
        borderColor: '#fff',
        pointBackgroundColor:'#fff',
        pointHoverBackgroundColor:'#fff',
        pointBorderColor :'#fff',
        pointHoverBorderColor :'#fff',
        pointBorderWidth :1,
        pointRadius :0,
        pointHoverRadius :4,
        borderWidth: 3
      }]
    }
    ,
    options: {
      legend: {
        position: false,
        display: true,
      },
      tooltips: {
        enabled: false
      },
      scales: {
        xAxes: [{
          display: false,
          gridLines: false
        }],
        yAxes: [{
          display: false,
          gridLines: false
        }]
      }
    }

  });

}
