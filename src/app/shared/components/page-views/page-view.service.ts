import {Injectable} from '@angular/core';
import {SharedApiService} from '../../share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageViewService {

  constructor(private shareApi: SharedApiService) {
  }

  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/dashboard/ecommerce/page-views');
  }
}
