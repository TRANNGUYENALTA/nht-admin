import {Component, OnInit} from '@angular/core';
import {ServerDataSource} from 'ng2-smart-table';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocalDataSource} from 'ng2-smart-table';
import swal from 'sweetalert';
import {BaseUrlService} from '@shared/base_url/base-url.service';

@Component({
  selector: 'app-smart-table',
  styleUrls: ['./smart-table.component.css'],
  templateUrl: './smart-table.component.html',
})
export class SmartTableComponent implements OnInit {
  settings = {
    pager: {
      display: true,
      perPage: 5, // config page limit
    },
    delete: {
      deleteButtonContent: '<i class="ion ion-md-trash"></i>',
      confirmDelete: true,
    },
    add: {
      addButtonContent: '<i class="ion ion-md-add"></i>',
      createButtonContent: '<i class="ion ion-md-checkmark-circle-outline"></i>',
      cancelButtonContent: '<i class="ion ion-md-close-circle-outline"></i>',
      confirmCreate: true
    },
    edit: {
      editButtonContent: '<i class="fas fa-pencil-alt"></i>',
      saveButtonContent: '<i class="far fa-check-square"></i>',
      cancelButtonContent: '<i class="ion ion-md-close-circle-outline"></i>',
      confirmSave: true
    },

    columns: {
      id: {
        title: 'ID',
        editable: false,
        filter: false
      },
      name: {
        title: 'Name',
      },
      position: {
        title: 'position',
      },
      office: {
        title: 'office',
      },
      age: {
        title: 'age',
      },
      start_date: {
        title: 'start_date',
      },
    },
  };


  source: ServerDataSource;

  constructor(httpClient: HttpClient,
              private base_url: BaseUrlService) {
    this.source = new ServerDataSource(httpClient, {endPoint: this.base_url.getBaseUrl() + '/api/table/data-tables/data-exportingv1'});  // write api here
  }

  // source: LocalDataSource;
  //
  // constructor() {
  //   this.source = new LocalDataSource(this.data);
  // }

  ngOnInit() {
  }

  onCreateConfirm(event) {
    console.log('add Record', event.newData);
    swal({
      title: 'Are you sure?',
      text: `You want to create?`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willCreate) => {
        if (willCreate) {
          event.confirm.resolve();
        } else {
          event.confirm.reject();
        }
      });
  }

  onSaveConfirm(event) {
    console.log('add Record', event.newData);
    swal({
      title: 'Are you sure?',
      text: `You want to Update!`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willEdit) => {
        if (willEdit) {
          event.confirm.resolve();
        } else {
          event.confirm.reject();
        }
      });
  }

  onDeleteConfirm(event) {
    console.log(event.data);
    swal({
      title: 'Are you sure?',
      text: `Once deleted, you will not be able to recover this user information!`,
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          console.log('send api delete');
          event.confirm.resolve();
        } else {
          swal('Your field is safe!');
          event.confirm.reject();
        }
      });
  }
}
