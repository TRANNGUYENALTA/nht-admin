import {Component, OnDestroy, OnInit} from '@angular/core';
import {BasicTableService} from './basic-table.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.css']
})
export class BasicTableComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  private datas: any = [];

  constructor(private basicTableService: BasicTableService) {
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.basicTableService.getDatas().subscribe(res => {
      this.datas = res.data;
    });
  }

  getDataAttribute() {
    return this.datas;
  }

}
