import {Component, OnInit} from '@angular/core';
import {BaseUrlService} from '../../../base_url/base-url.service';

declare var $: any;

@Component({
  selector: 'app-data-table-example',
  templateUrl: './data-table-example.component.html',
  styleUrls: ['./data-table-example.component.css']
})
export class DataTableExampleComponent implements OnInit {

  constructor(private baseUrl: BaseUrlService) {
  }

  ngOnInit() {
    this.drawTable();
  }
  drawTable(){
    const that = this;
    // Default data table
    $('#default-datatable').DataTable(
      {
        processing: true,
        'pageLength': 10,
        'serverSide': true,
        ajax: {
          url: that.baseUrl.getBaseUrl() + '/api/table/data-tables/data-table-example',
        },
        'drawCallback': function (settings) {
        },
        columns: [
          {data: 'id', name: 'id', 'title': '#'},
          {data: 'name', name: 'name', 'title': 'Name'},
          {data: 'position', name: 'position', 'title': 'Position'},
          {data: 'office', name: 'office', 'title': 'Office'},
          {data: 'age', name: 'age', 'title': 'Age'},
          {data: 'start_date', name: 'start_date', 'title': 'Start Date'},
          {data: 'salary', name: 'salary', 'title': 'Salary'},
        ],
        columnDefs: [
          // {
          //   'targets': 0,
          //   'searchable': false,
          //   'orderable': false,
          //   'render': function (data, type, row) {
          //     return `<div class=" btn btn-success">` + row.id + `</div>`;
          //   }
          // }
        ]
      }
    );
  }
}
