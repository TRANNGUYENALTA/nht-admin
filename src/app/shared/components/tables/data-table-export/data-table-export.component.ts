import {Component, OnInit} from '@angular/core';
import {BaseUrlService} from '../../../base_url/base-url.service';
declare var $: any;

@Component({
  selector: 'app-data-table-export',
  templateUrl: './data-table-export.component.html',
  styleUrls: ['./data-table-export.component.css']
})
export class DataTableExportComponent implements OnInit {

  constructor(private baseUrl: BaseUrlService) {
  }

  ngOnInit() {
    this.drawTable();
  }

  drawTable() {
    const that = this;
    const table = $('#example').DataTable({
      lengthChange: false,
      buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
      processing: true,
      'pageLength': 10,
      'serverSide': true,
      ajax: {
        url: that.baseUrl.getBaseUrl() + '/api/table/data-tables/data-table-example',
      },
      'drawCallback': function (settings) {
        setTimeout(function () {
          table.buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
        });
      },
      columns: [
        {data: 'id', name: 'id', 'title': '#'},
        {data: 'name', name: 'name', 'title': 'Name'},
        {data: 'position', name: 'position', 'title': 'Position'},
        {data: 'office', name: 'office', 'title': 'Office'},
        {data: 'age', name: 'age', 'title': 'Age'},
        {data: 'start_date', name: 'start_date', 'title': 'Start Date'},
        {data: 'salary', name: 'salary', 'title': 'Salary'},
      ],
      columnDefs: [
        // {
        //   'targets': 0,
        //   'searchable': false,
        //   'orderable': false,
        //   'render': function (data, type, row) {
        //     return `<div class=" btn btn-success">` + row.id + `</div>`;
        //   }
        // }
      ]
    });

  }
}
