import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SharedApiService} from '@shared/share_service/shared-api.service';

@Injectable({
  providedIn: 'root'
})
export class TotalCounterService {

  constructor(private shareApi: SharedApiService) {
  }

  getList(): Observable<any> {
    return this.shareApi.getApi('/api/dashboard/ecommerce/cart-ecommerce');
  }
}
