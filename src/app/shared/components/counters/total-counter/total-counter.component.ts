import {Component, OnDestroy, OnInit} from '@angular/core';
import {TotalCounterService} from './total-counter.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-total-counter',
  templateUrl: './total-counter.component.html',
  styleUrls: ['./total-counter.component.css']
})
export class TotalCounterComponent implements OnInit, OnDestroy {
  private _unsubscribe: Subscription = new Subscription();

  constructor(private totalCounterService: TotalCounterService) {
  }

  data: any = [];

  ngOnInit() {
    this.getList();
  }

  ngOnDestroy() {
    this._unsubscribe.unsubscribe();
  }

  getList() {
    this._unsubscribe = this.totalCounterService.getList().subscribe(res => {
      this.data = res.data;
    });
  }

}
