import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalCounterComponent } from './statistical-counter.component';

describe('StatisticalCounterComponent', () => {
  let component: StatisticalCounterComponent;
  let fixture: ComponentFixture<StatisticalCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticalCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticalCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
