import {Component, OnInit, Input} from '@angular/core';
// enum dataDirection {
//   'Total Orders' = 'fa fa-shopping-cart',
// }
@Component({
  selector: 'app-statistical-counter',
  templateUrl: './statistical-counter.component.html',
  styleUrls: ['./statistical-counter.component.css']
})

export class StatisticalCounterComponent implements OnInit {
  @Input() title: string;
  @Input() totalNumber: string;
  @Input() changePercent: string;
  @Input() currentPercent: string;
  public iconClass = '';
  constructor() {
  }

  ngOnInit() {
    this.setIconClass();
  }

  setIconClass() {
    switch (this.title) {
      case 'Total Orders': {
        this.iconClass = 'fa fa-shopping-cart';
        break;
      }
      case 'Total Revenue': {
        this.iconClass = 'fa fa-usd';
        break;
      }
      case 'Visitors': {
        this.iconClass = 'fa fa-eye';
        break;
      }
      case 'Messages': {
        this.iconClass = 'fa fa-envira';
        break;
      }
      default: {
        this.iconClass = 'fa fa-shopping-cart';
        break;
      }
    }
  }
}
