import {Component, OnDestroy, OnInit} from '@angular/core';
import '@assets/plugins/jquery-knob/jquery.knob.js';
import {drawKnobChart} from './chart';
import {Subscription} from 'rxjs';
import {KnobChartService} from '@shared/components/jquery-knob-chart/knob-chart.service';

@Component({
  selector: 'app-jquery-knob-chart',
  templateUrl: './jquery-knob-chart.component.html',
  styleUrls: ['./jquery-knob-chart.component.css']
})
export class JqueryKnobChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  public datas: any = [{}];

  constructor(private knodChartService: KnobChartService) {
  }

  ngOnInit() {
    this.getData();
    drawKnobChart();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.knodChartService.getData().subscribe(res => {
      console.log(res.data.chart);
      this.datas = res.data.chart;
      console.log(this.datas);
    });
  }

}

