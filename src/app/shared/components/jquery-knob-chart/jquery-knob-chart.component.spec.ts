import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqueryKnobChartComponent } from './jquery-knob-chart.component';

describe('JqueryKnobChartComponent', () => {
  let component: JqueryKnobChartComponent;
  let fixture: ComponentFixture<JqueryKnobChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqueryKnobChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqueryKnobChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
