import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EasyPieChartsService {

  constructor(private shareApi: SharedApiService) {
  }

  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/chart/other-chart/jquery-easy-pie-chart');
  }
}
