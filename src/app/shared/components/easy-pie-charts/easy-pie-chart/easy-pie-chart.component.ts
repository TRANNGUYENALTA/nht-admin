import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawEasyPieChart} from './chart';

@Component({
  selector: 'app-easy-pie-chart',
  templateUrl: './easy-pie-chart.component.html',
  styleUrls: ['./easy-pie-chart.component.css']
})
export class EasyPieChartComponent implements OnInit, OnDestroy {

  @Input() percent: number;

  constructor() {
  }

  ngOnInit() {
    setTimeout(function () {
      drawEasyPieChart();
    });
  }

  ngOnDestroy() {
  }




}
