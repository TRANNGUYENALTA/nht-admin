import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyPieChartComponent } from './easy-pie-chart.component';

describe('EasyPieChartComponent', () => {
  let component: EasyPieChartComponent;
  let fixture: ComponentFixture<EasyPieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyPieChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
