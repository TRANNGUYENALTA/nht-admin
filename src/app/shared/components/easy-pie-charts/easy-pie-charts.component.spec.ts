import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyPieChartsComponent } from './easy-pie-charts.component';

describe('EasyPieChartsComponent', () => {
  let component: EasyPieChartsComponent;
  let fixture: ComponentFixture<EasyPieChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyPieChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyPieChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
