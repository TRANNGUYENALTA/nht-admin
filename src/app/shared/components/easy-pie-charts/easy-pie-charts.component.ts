import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {EasyPieChartsService} from '@shared/components/easy-pie-charts/easy-pie-charts.service';

@Component({
  selector: 'app-easy-pie-charts',
  templateUrl: './easy-pie-charts.component.html',
  styleUrls: ['./easy-pie-charts.component.css']
})
export class EasyPieChartsComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  constructor(private easyPieChartService: EasyPieChartsService) {
  }

  datas: any = [];

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.easyPieChartService.getDatas().subscribe(res => {
      this.datas = res.data.chart;
    });
  }
}
