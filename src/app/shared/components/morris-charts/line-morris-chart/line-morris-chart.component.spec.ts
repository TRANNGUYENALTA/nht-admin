import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineMorrisChartComponent } from './line-morris-chart.component';

describe('LineMorrisChartComponent', () => {
  let component: LineMorrisChartComponent;
  let fixture: ComponentFixture<LineMorrisChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineMorrisChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineMorrisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
