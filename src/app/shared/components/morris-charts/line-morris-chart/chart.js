export function drawLineChartMorris(data) {
  // mock data
  // 0: {x: "2011 Q1", y: "1", z: "0"}
// 1: {x: "2011 Q2", y: "6", z: "2"}
// 2: {x: "2011 Q3", y: "2", z: "6"}
// 3: {x: "2011 Q4", y: "8", z: "1"}
  Morris.Area({
    element: 'line-chart', // id element
    behaveLikeLine: true,
    data: convertData(data),
    xkey: 'x',
    ykeys:  getYkeys(data[0]),
    labels: getArrayLable(data[0].data),
    lineColors: ['#fba540', '#03d0ea'],
    resize: true,
    gridTextColor : "#ddd",
    fillOpacity: 0.1,
  });
}


function convertData(data) {
  var tempArr = [];
  data.forEach((value, index) => {
    tempArr.push(creatRowObject(value))
  })
  return tempArr;
}

function getYkeys(data) {
  var yKeys = []
  data.data.forEach((value, index) => {
    yKeys.push('key' + index)
  })
  return yKeys;
}

function creatRowObject(data) { //{x: '2011 Q1', y: 3, z: 2, a: 1, t: 2},
  var rowObj = {}
  rowObj = {...rowObj, 'x': data.quarter}
  data.data.forEach((value, index) => {
    let tempObject = {};
    tempObject['key' + index] = value.value; // dynamic object key value
    rowObj = {...rowObj, ...tempObject}
  })
  return rowObj;
}
function getArrayLable(data =[]){
  var temp = [];
  data.forEach((value, index) => {
    temp.push(value.name);
  });
  return temp;
}

