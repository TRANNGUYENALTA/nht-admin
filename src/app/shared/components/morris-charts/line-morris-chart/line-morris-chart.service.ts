import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SharedApiService} from '@shared/share_service/shared-api.service';

@Injectable({
  providedIn: 'root'
})
export class LineMorrisChartService {

  constructor(private shareApi: SharedApiService) {
  }

  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/chart/morris-charts/line-chart');
  }
}
