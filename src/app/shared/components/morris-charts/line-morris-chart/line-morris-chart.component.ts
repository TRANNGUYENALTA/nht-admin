import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawLineChartMorris} from './chart';
import {LineMorrisChartService} from '@shared/components/morris-charts/line-morris-chart/line-morris-chart.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-line-morris-chart',
  templateUrl: './line-morris-chart.component.html',
  styleUrls: ['./line-morris-chart.component.css']
})
export class LineMorrisChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();


  constructor(private lineMorrisService: LineMorrisChartService) {
  }

  ngOnInit() {
    this.getData();

  }

  getData() {
    this._subscription = this.lineMorrisService.getDatas().subscribe(res => {
      drawLineChartMorris(res.data.line);
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
