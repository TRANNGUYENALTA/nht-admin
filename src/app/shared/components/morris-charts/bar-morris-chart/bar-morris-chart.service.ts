import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BarMorrisChartService {

  constructor(private shareApi: SharedApiService) {
  }

  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/chart/morris-charts/bar-chart');
  }
}
