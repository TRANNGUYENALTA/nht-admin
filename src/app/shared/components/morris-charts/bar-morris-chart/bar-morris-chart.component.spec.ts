import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarMorrisChartComponent } from './bar-morris-chart.component';

describe('BarMorrisChartComponent', () => {
  let component: BarMorrisChartComponent;
  let fixture: ComponentFixture<BarMorrisChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarMorrisChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarMorrisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
