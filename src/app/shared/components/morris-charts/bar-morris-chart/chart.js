export function drawBarChartMorris(data) {
  // var  mockdata=  [
  //    {x: '2011 Q1', y: 3, z: 2, a: 1, t: 2},
  //    {x: '2011 Q2', y: 2, z: 3, a: 1, t: 2},
  //    {x: '2011 Q3', y: 1, z: 2, a: 4, t: 2},
  //    {x: '2011 Q4', y: 2, z: 4, a: 3, t: 2},
  //    {x: '2011 Q4', y: 2, z: 4, a: 3, t: 2}
  //  ]

  // Use Morris.Bar
  Morris.Bar({
    element: 'bar-chart',
    data: convertData(data),//
    xkey: 'x',
    ykeys: getYkeys(data[0]),
    labels: getArrayLable(data[0].data), //
    barColors: ['#03d0ea', '#d13adf', '#fba540', '#fba540'],
    gridTextColor: "#ddd",
    resize: true
  });

}

function convertData(data) {
  console.log(data)
  var tempArr = [];
  data.forEach((value, index) => {
    tempArr.push(creatRowObject(value))
  })
  return tempArr;
}

function getYkeys(data)
{
  var yKeys = []
  data.data.forEach((value, index) => {
    yKeys.push('key' + index)
  })
  return yKeys;
}

function creatRowObject(data) { //{x: '2011 Q1', y: 3, z: 2, a: 1, t: 2},
  var rowObj = {}
  rowObj = {...rowObj, 'x': data.date_value}
  data.data.forEach((value, index) => {
    let tempObject = {};
    tempObject['key' + index] = value.value; // dynamic object key value
    rowObj = {...rowObj, ...tempObject}
  })
  return rowObj;
}


function getArrayLable(data) {
  var labels = [];
  data.forEach((value, index) => {
    labels.push(value.name);
  });
  return labels;
}
