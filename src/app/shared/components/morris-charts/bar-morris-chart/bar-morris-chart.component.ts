import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawBarChartMorris} from './chart';
import {BarMorrisChartService} from '@shared/components/morris-charts/bar-morris-chart/bar-morris-chart.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-bar-morris-chart',
  templateUrl: './bar-morris-chart.component.html',
  styleUrls: ['./bar-morris-chart.component.css']
})
export class BarMorrisChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  constructor(private barMorrisChartService: BarMorrisChartService) {
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.barMorrisChartService.getDatas().subscribe(res => {
      drawBarChartMorris(res.data.column);
    });
  }
}
