import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawDonutChartMorris} from './chart';
import {Subscription} from 'rxjs';
import {DonutMorrisChartService} from '@shared/components/morris-charts/donut-morris-chart/donut-morris-chart.service';

@Component({
  selector: 'app-donut-morris-chart',
  templateUrl: './donut-morris-chart.component.html',
  styleUrls: ['./donut-morris-chart.component.css']
})
export class DonutMorrisChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  ngOnInit() {
    this.getData();

  }
  constructor(private donutChartService: DonutMorrisChartService) {
  }

  getData() {
    this._subscription = this.donutChartService.getDatas().subscribe(res => {
      drawDonutChartMorris(res.data.data);
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
