import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonutMorrisChartComponent } from './donut-morris-chart.component';

describe('DonutMorrisChartComponent', () => {
  let component: DonutMorrisChartComponent;
  let fixture: ComponentFixture<DonutMorrisChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonutMorrisChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonutMorrisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
