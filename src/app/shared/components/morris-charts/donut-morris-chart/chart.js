export function drawDonutChartMorris(data) {
  Morris.Donut({
    element: 'donut-chart', // get id element
    data: convertData(data),
    colors: [
      '#14abef',
      '#eb5076',
      '#03d0ea',
      '#02ba5a'
    ],
    resize: true,
    labelColor: "#ffffff",
    formatter: function (x) {
      return x + "%"
    }
  });
}

function convertData(data) {
  var tempArrr = [];
  data.forEach((value, index) => {
    tempArrr.push({
        value: value.value,
        label: value.name
      }
    )
  })
  return tempArrr;
}
