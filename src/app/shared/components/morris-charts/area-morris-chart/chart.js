export function drawAreaChartMorris(data) {
  convertData(data);
  getArrayLable(data[0].data)
  Morris.Area({
    element: 'area-chart',
    data: convertData(data),
    xkey: 'x',
    ykeys: getYkeys(data[0]),
    labels: getArrayLable(data[0].data),
    lineColors: ['#eb5076', '#14abef'],
    gridTextColor: "#ddd",
    resize: true
  });
}


function convertData(data) {
  var tempArr = [];
  data.forEach((value, index) => {
    tempArr.push(creatRowObject(value))
  })
  return tempArr;
}

function getYkeys(data) {
  var yKeys = []
  data.data.forEach((value, index) => {
    yKeys.push('key' + index)
  })
  return yKeys;
}

function creatRowObject(data) { //{x: '2011 Q1', y: 3, z: 2, a: 1, t: 2},
  var rowObj = {}
  rowObj = {...rowObj, 'x': data.quarter}
  data.data.forEach((value, index) => {
    let tempObject = {};
    tempObject['key' + index] = value.value; // dynamic object key value
    rowObj = {...rowObj, ...tempObject}
  })
  return rowObj;
}

function getArrayLable(data = []) {
  var temp = [];
  data.forEach((value, index) => {
    temp.push(value.name);
  });

  return temp;
}
