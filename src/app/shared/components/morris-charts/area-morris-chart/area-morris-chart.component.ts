import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawAreaChartMorris} from './chart';
import {Subscription} from 'rxjs';
import {AreaMorrisChartService} from '@shared/components/morris-charts/area-morris-chart/area-morris-chart.service';

@Component({
  selector: 'app-area-morris-chart',
  templateUrl: './area-morris-chart.component.html',
  styleUrls: ['./area-morris-chart.component.css']
})
export class AreaMorrisChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  constructor(private areaMorrisService: AreaMorrisChartService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._subscription = this.areaMorrisService.getDatas().subscribe(res => {
      drawAreaChartMorris(res.data.line);
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
