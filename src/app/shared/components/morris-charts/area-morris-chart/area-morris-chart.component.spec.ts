import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaMorrisChartComponent } from './area-morris-chart.component';

describe('AreaMorrisChartComponent', () => {
  let component: AreaMorrisChartComponent;
  let fixture: ComponentFixture<AreaMorrisChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaMorrisChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaMorrisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
