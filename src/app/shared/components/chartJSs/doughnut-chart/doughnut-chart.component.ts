import {Component, Input, OnDestroy, OnInit} from '@angular/core';
// import './../../../../..//assets/plugins/Chart.js/Chart.min.js';  //import independence
import {drawDouhtnutChart} from './chart';
import {DoughbutChartService} from '@shared/components/chartJSs/doughnut-chart/doughbut-chart.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  constructor(private doughtChartService: DoughbutChartService) {
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.doughtChartService.getData().subscribe(res => {
      drawDouhtnutChart(res.data[0].data);
    });
  }
}
