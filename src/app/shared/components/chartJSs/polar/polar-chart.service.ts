import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable, ObservableInput} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PolarChartService {

  constructor(private shareApi: SharedApiService) {
  }

  getDatas(): Observable<any> {
    return this.shareApi.getApi('/api/chart/chart-js/polar-chart');
  }
}
