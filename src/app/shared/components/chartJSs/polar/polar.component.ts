import {Component, Input, OnDestroy, OnInit} from '@angular/core';
// import './../../../../..//assets/plugins/Chart.js/Chart.min.js';  //import independence
import {drawPolarChart} from './chart';
import {PolarChartService} from '@shared/components/chartJSs/polar/polar-chart.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-polar',
  templateUrl: './polar.component.html',
  styleUrls: ['./polar.component.css']
})
export class PolarComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();


  constructor(private polarChartService: PolarChartService) {
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.polarChartService.getDatas().subscribe(res => {
      drawPolarChart(res.data.data);
    });
  }

}
