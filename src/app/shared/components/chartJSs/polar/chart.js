export function drawPolarChart( data) {
  if ($('#polarChart').length) {
    var ctx = document.getElementById("polarChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'polarArea',
      data: {
        labels: convertArray(data, 'key'),
        datasets: [{
          backgroundColor: [
            random_rgba(),random_rgba(),random_rgba(),random_rgba()
            // "rgba(255, 255, 255, 0.35)",
            // "#ffffff",
            // "rgba(255, 255, 255, 0.12)",
            // "rgba(255, 255, 255, 0.71)"
          ],
          data: convertArray(data, 'value'),
          borderWidth: [0, 0, 0, 0]
        }]
      },
      options: {
        legend: {
          position :"right",
          display: true,
          labels: {
            fontColor: '#ddd',
            boxWidth:15
          }
        },
        scale: {
          gridLines: {
            color: "rgba(221, 221, 221, 0.12)"
          },
        }
      }
    });
  }
}
function convertArray(data, type) {
  var tempArr = [];
  data.forEach((value, index) => {
    if (type == 'key') {
      tempArr.push(value.name )
    } else {
      tempArr.push(value.value)
    }
  })
  return tempArr;
}
function random_rgba() {
  var o = Math.round, r = Math.random, s = 255;
  return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
}
