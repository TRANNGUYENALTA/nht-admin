export function drawChart(data) {
  // chart Char js type line
  var ctx = document.getElementById('chart1').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: convertArray(data[0].data, 'key'),
      datasets: convertDatasetsChart(data)
    },
    options: {
      legend: {
        display: true,
        labels: {
          fontColor: '#ddd',
          boxWidth: 40
        }
      },
      tooltips: {
        displayColors: true
      },
      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: '#ddd'
          },
          gridLines: {
            display: true,
            color: "rgba(221, 221, 221, 0.08)"
          },
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: '#ddd'
          },
          gridLines: {
            display: true,
            color: "rgba(221, 221, 221, 0.08)"
          },
        }]
      }

    }
  });
}
function convertArray(data, type) {
  var tempArr = [];
  data.forEach((value, index) => {
    if (type == 'key') {
      tempArr.push(value.date_value )
    } else {
      tempArr.push(value.value)
    }
  })
  return tempArr;
}

function random_rgba() {
  var o = Math.round, r = Math.random, s = 255;
  return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
}

function convertDatasetsChart(data) {
  var datasets = [];
  data.forEach((value, index) => {
    datasets.push({
      label: value.name,
      data: convertArray(value.data, 'value'),
      backgroundColor: random_rgba(),
      borderColor: "transparent",
      pointRadius: "0",
      borderWidth: 3
    })
    }
  )
  return datasets;
}
