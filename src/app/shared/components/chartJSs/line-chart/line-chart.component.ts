import {Component, Input, OnDestroy, OnInit} from '@angular/core';
// import './../../../../..//assets/plugins/Chart.js/Chart.min.js';  //import independence
import {drawChart} from './chart';
import {LineChartjsService} from '@shared/components/chartJSs/line-chart/line-chartjs.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  private datas: any = [];


  constructor(private linChartjsService: LineChartjsService) {
  }

  ngOnInit() {
    this.getData();
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.linChartjsService.getDatas().subscribe(res => {
      this.datas = res.data.line;
      drawChart(this.datas);
    });

  }

}
