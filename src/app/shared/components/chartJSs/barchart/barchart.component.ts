import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {drawBarChart} from './chart.js';
// import './../../../../../assets/plugins/Chart.js/Chart.min.js';  //import independence
import {BarChartjsService} from '@shared/components/chartJSs/barchart/bar-chartjs.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css']
})
export class BarchartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  datas: any = [];
  constructor(private barChartService: BarChartjsService) {
  }

  ngOnInit() {
    this.getData();

  }
  ngOnDestroy(){
    this._subscription.unsubscribe();
  }
  getData() {
    this._subscription = this.barChartService.getData().subscribe(res => {
      this.datas = res.data.line;
       drawBarChart(this.datas);
    });
  }

}
