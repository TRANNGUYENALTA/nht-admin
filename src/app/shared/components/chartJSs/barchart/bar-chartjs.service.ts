import {Injectable} from '@angular/core';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BarChartjsService {

  constructor(private sharApi: SharedApiService) {
  }

  getData(): Observable<any> {
    return this.sharApi.getApi('/api/chart/chart-js/bar-chart');
  }
}
