export function drawBarChart(data) {
  if ($('#barChart').length) {
    var ctx = document.getElementById("barChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: convertArray(data[0].data, 'key'),
        datasets: convertDatasetsChart(data)
      },
      options: {
        legend: {
          display: true,
          labels: {
            fontColor: '#ddd',
            boxWidth: 40
          }
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            barPercentage: .5,
            ticks: {
              beginAtZero: true,
              fontColor: '#ddd'
            },
            gridLines: {
              display: true,
              color: "rgba(221, 221, 221, 0.08)"
            },
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: '#ddd'
            },
            gridLines: {
              display: true,
              color: "rgba(221, 221, 221, 0.08)"
            },
          }]
        }

      }
    });
  }
}
function convertArray(data, type) {
  var tempArr = [];
  data.forEach((value, index) => {
    if (type == 'key') {
      tempArr.push(value.date_value )
    } else {
      tempArr.push(value.value)
    }
  })
  return tempArr;
}

function random_rgba() {
  var o = Math.round, r = Math.random, s = 255;
  return 'rgba(255 ,255, 255,' + r().toFixed(1) + ')';
}

function convertDatasetsChart(data) {
  var datasets = [];
  data.forEach((value, index) => {
      datasets.push({
        label: value.name,
        data: convertArray(value.data, 'value'),
        backgroundColor: random_rgba()
      })
    }
  )
  return datasets;
}
