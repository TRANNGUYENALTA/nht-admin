import {Component, Input, OnDestroy, OnInit} from '@angular/core';
// import './../../../../..//assets/plugins/Chart.js/Chart.min.js';  //import independence
import {drawPieChart} from './chart';
import {PieChartjsService} from '@shared/components/chartJSs/piechart/pie-chartjs.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})
export class PiechartComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  constructor(private pieChartjsService: PieChartjsService) {
  }

  ngOnInit() {
    this.getData();

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  getData() {
    this._subscription = this.pieChartjsService.getDatas().subscribe(res => {
      drawPieChart(res.data.data);
    });
  }

}
