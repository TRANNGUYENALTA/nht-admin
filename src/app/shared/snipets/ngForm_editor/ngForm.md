<div class="wrap-login h-100 d-flex align-items-center">
  <form class="form-signin pt-4" #formLogin="ngForm" (ngSubmit)="onSignIn(formLogin)">
    <div class="mb-4 logo text-uppercase w-50 m-auto">
      <div class="brands-name text-capitalize">Template</div>
      <span class="sologan text-uppercase d-block">ppc Admin portal</span>
    </div>
    <div class="form-label-group mt-5">
      <input type="email"
             name="email"
             #inputEmail='ngModel'
             class="form-control"
             placeholder="Email"
             [(ngModel)]="user.userName"
             autofocus=""
             required
             pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
      >
      <div *ngIf="inputEmail.invalid && (inputEmail.dirty || inputEmail.touched)">
        <div *ngIf="inputEmail?.errors.required" class="text-danger"> Email Adress is required.</div>
        <div *ngIf="inputEmail?.errors.pattern" class="text-danger"> Email Wrong.</div>
      </div>
    </div>

    <div class="form-label-group my-3 mb-5">
      <input type="password"
             #inputPassword='ngModel'
             name="password"
             class="form-control"
             [(ngModel)]="user.passWord"
             placeholder="Password"
             required
             maxlength="10"
      >
      <div *ngIf="inputPassword.invalid && (inputPassword.dirty || inputPassword.touched)">
        <div *ngIf="inputPassword?.errors.required" class="text-danger"> Password is required.</div>
      </div>
    </div>
    <!--<div class="form-label-group my-3 mb-5">-->
      <!--<textarea type="password"-->
                <!--#textArea='ngModel'-->
                <!--name="description"-->
                <!--class="form-control"-->
                <!--[(ngModel)]="textarea"-->
                <!--placeholder="Description"-->
      <!--&gt;</textarea>-->
    <!--</div>-->
    <!--<div class="form-label-group my-3 mb-5">-->
      <!--<select name="select" #select='ngModel' [(ngModel)]="selected">-->
        <!--<option value="1">1</option>-->
        <!--<option value="2">2</option>-->
        <!--<option value="3">3</option>-->
        <!--<option value="4">4</option>-->
        <!--<option value="5">5</option>-->
      <!--</select>-->
    <!--</div>-->
    <!--<app-ngx-editor [placeholder]="'Enter text here...'" [spellcheck]="true" [(ngModel)]="htmlContent" name="editor" ></app-ngx-editor>-->
    <button class="btn btn-lg  btn-block  text-capitalize" type="submit"
            [disabled]="formLogin.invalid"> LOG IN
    </button>
    <button class="btn btn-info text-capitalize" (click)="resetForm()" type="button"> Reset</button>
    <button class="btn btn-info text-capitalize" (click)="setForm()" type="button"> set Form</button>
    <a routerLink="/register" class="mb-5 d-block mt-3 text-light"> Register</a>
  </form>
</div>  
//============================================================================== Login Component ====================================================================================
import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../models/user';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('formLogin') vlForm;
  user: User = new User();
  htmlContent: any;

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
  }

  onSignIn(f: NgForm) {
    // console.log(f.form.value);
    console.log(this.vlForm.value);
    this.auth.login(this.vlForm.value);
  }

  resetForm() {
    this.vlForm.reset();
  }

  setForm() {
    this.vlForm.setValue({email: 'superadmin@gmail.com', password: '123456'});
  }
}
