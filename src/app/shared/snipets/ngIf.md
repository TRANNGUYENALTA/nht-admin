: ====> .html
<div *ngIf="isShow;else elseBlock">show data</div>
<ng-template #elseBlock>loading... </ng-template>
: ====> .component.ts
isShow = true
