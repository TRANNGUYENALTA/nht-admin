/*Sử dụng  Attribute Directive
* B1: ClI: ng generate directive nameDirective  || ng g d nameDirect
* B2:  Import và export trong shared module
* B3: Sử dụng  '<div appHightLight [defaultColor]="'brown'" >Hight Light directives binding from shared module</div>'*/
import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appHightLight]'
})
export class HightLightDirective implements OnInit {
  @Input() defaultColor: string;
  @Input() higihtLightColor: string;
  @HostBinding('style.backgroundColor') backgroundColor: any ; // Bind kiểu này có thể dùng một cách trực tiếp

  constructor(
    private elRef: ElementRef,
    private render2: Renderer2
  ) {}

  ngOnInit() {
    // console.log(this.defaultColor);
    // console.log(this.higihtLightColor);
    // console.log(this.elRef);
    this.elRef.nativeElement.style.backgroundColor = this.higihtLightColor;
    this.render2.addClass(this.elRef.nativeElement, 'basicClass');  // Sử dụng đối tượng render2 để thao tác elementRef
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.hightLight(this.defaultColor || 'red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.hightLight( this.higihtLightColor);
  }

  hightLight(_color) {
    this.elRef.nativeElement.style.backgroundColor = _color;
  }

}
