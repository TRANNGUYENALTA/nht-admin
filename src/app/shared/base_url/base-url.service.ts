import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {
  private BASE_URL = 'http://192.168.11.33:8080';

  getBaseUrl() {
    return this.BASE_URL;
  }
  constructor() {
  }
}
// http://192.168.11.36
