import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AuthRoutingModule} from './auth-routing.module';
import {AuthComponent} from './auth.component';
import {NgxEditorModule} from 'ngx-editor';
import {SharedModule} from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    FormsModule,
    NgxEditorModule,
  ],
  declarations: [
    AuthComponent,
  ]
})
export class AuthModule {
}
