import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {AuthComponent} from './auth.component';
import {RegisterComponent} from '../../shared/components';
import {SignIn1Component} from '@shared/components/auth/sign-in1/sign-in1.component';

const routes: Routes = [
  { path: 'users', component: AuthComponent ,
    children: [
      {
        path: '', redirectTo: 'login', pathMatch: 'full'
      },
      {
        path: 'login',
        component: SignIn1Component,
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
      { path: '**', redirectTo: 'login' }
    ]},
  {path: 'signIn-1', loadChildren: './components/sigin1/sigin1.module#Sigin1Module'},
  {path: 'signIn-2', loadChildren: './components/sign-in2/sign-in2.module#SignIn2Module'},
  {path: 'signUp-1', loadChildren: './components/sign-up1/sign-up1.module#SignUp1Module'},
  {path: 'signUp-2', loadChildren: './components/sign-up2/sign-up2.module#SignUp2Module'},
  {path: 'lock-screen', loadChildren: './components/lock-screen/lock-screen.module#LockScreenModule'},
  {path: 'reset-password-1', loadChildren: './components/reset-password1/reset-password1.module#ResetPassword1Module'},
  {path: 'reset-password-2', loadChildren: './components/reset-password2/reset-password2.module#ResetPassword2Module'},
  { path: '**', redirectTo: 'users', pathMatch: 'full' },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
