import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Sigin1RoutingModule } from './sigin1-routing.module';
import {SharedModule} from '@shared/shared.module';
import { SignIn1Component } from '../../../../shared/components/auth/sign-in1/sign-in1.component';

@NgModule({
  declarations: [SignIn1Component],
  imports: [
    CommonModule,
    SharedModule,
    Sigin1RoutingModule
  ]
})
export class Sigin1Module { }
