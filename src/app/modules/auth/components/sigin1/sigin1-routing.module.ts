import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignIn1Component} from '@shared/components/auth/sign-in1/sign-in1.component';

const routes: Routes = [
  {path: '', component: SignIn1Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Sigin1RoutingModule { }
