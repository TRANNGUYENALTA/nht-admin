import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignIn2RoutingModule } from './sign-in2-routing.module';
import { SignIn2Component } from '../../../../shared/components/auth/sign-in2/sign-in2.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [SignIn2Component],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    SignIn2RoutingModule
  ]
})
export class SignIn2Module { }
