import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignIn2Component} from '../../../../shared/components/auth/sign-in2/sign-in2.component';

const routes: Routes = [{
  path: '', component: SignIn2Component
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignIn2RoutingModule {
}
