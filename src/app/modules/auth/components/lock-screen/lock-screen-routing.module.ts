import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LockScreenComponent} from '../../../../shared/components/auth/lock-screen/lock-screen.component';

const routes: Routes = [
  {path: '', component: LockScreenComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LockScreenRoutingModule {
}
