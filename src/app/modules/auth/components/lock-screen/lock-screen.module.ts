import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LockScreenRoutingModule } from './lock-screen-routing.module';
import { LockScreenComponent } from '../../../../shared/components/auth/lock-screen/lock-screen.component';

@NgModule({
  declarations: [LockScreenComponent],
  imports: [
    CommonModule,
    LockScreenRoutingModule
  ]
})
export class LockScreenModule { }
