import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResetPassword1Component} from '../../../../shared/components/auth/reset-password1/reset-password1.component';

const routes: Routes = [
  {path: '', component: ResetPassword1Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetPassword1RoutingModule { }
