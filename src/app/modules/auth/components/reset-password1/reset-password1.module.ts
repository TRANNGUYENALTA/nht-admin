import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetPassword1RoutingModule } from './reset-password1-routing.module';
import { ResetPassword1Component } from '../../../../shared/components/auth/reset-password1/reset-password1.component';

@NgModule({
  declarations: [ResetPassword1Component],
  imports: [
    CommonModule,
    ResetPassword1RoutingModule
  ]
})
export class ResetPassword1Module { }
