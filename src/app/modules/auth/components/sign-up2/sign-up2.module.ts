import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignUp2RoutingModule } from './sign-up2-routing.module';
import { SignUp2Component } from '../../../../shared/components/auth/sign-up2/sign-up2.component';
import {SharedModule} from '@shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [SignUp2Component],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    SignUp2RoutingModule
  ]
})
export class SignUp2Module { }
