import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignUp2Component} from '../../../../shared/components/auth/sign-up2/sign-up2.component';

const routes: Routes = [
  {path: '', component: SignUp2Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignUp2RoutingModule {
}
