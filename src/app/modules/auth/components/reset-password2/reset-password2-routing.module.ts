import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResetPassword2Component} from '../../../../shared/components/auth/reset-password2/reset-password2.component';

const routes: Routes = [
  {path: '', component: ResetPassword2Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResetPassword2RoutingModule { }
