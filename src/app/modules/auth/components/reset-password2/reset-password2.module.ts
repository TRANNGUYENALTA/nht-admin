import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResetPassword2RoutingModule } from './reset-password2-routing.module';
import { ResetPassword2Component } from '../../../../shared/components/auth/reset-password2/reset-password2.component';

@NgModule({
  declarations: [ResetPassword2Component],
  imports: [
    CommonModule,
    ResetPassword2RoutingModule
  ]
})
export class ResetPassword2Module { }
