import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignUp1RoutingModule } from './sign-up1-routing.module';
import { SignUp1Component } from '../../../../shared/components/auth/sign-up1/sign-up1.component';
import {SharedModule} from '../../../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [SignUp1Component],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    SignUp1RoutingModule
  ]
})
export class SignUp1Module { }
