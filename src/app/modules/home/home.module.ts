import {NgModule} from '@angular/core';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {HeaderComponent} from '../../shared/components/header/header.component';
// import third party
import {ProgressbarModule} from 'ngx-bootstrap';
import {SidebarMenuComponent} from '../../shared/components/sidebar-menu/sidebar-menu.component';
import {FooterComponent} from '../../shared/components/footer/footer.component';
import {BackToTopComponent} from '../../shared/components/back-to-top/back-to-top.component';

@NgModule({
  imports: [
    HomeRoutingModule,
    ProgressbarModule.forRoot(),

  ],
  declarations: [
    HomeComponent,
    HeaderComponent,
    SidebarMenuComponent,
    FooterComponent,
    BackToTopComponent
  ]
})
export class HomeModule {
}
