import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularNpmsRoutingModule } from './angular-npms-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AngularNpmsRoutingModule
  ]
})
export class AngularNpmsModule { }
