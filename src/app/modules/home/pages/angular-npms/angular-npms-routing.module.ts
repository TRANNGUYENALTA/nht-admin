import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: 'angular-editor-fabric', loadChildren: './submenus/angular-editor-fabric/angular-editor-fabric.module#AngularEditorFabricModule'},
  {path: 'color-pickers', loadChildren: './submenus/color-pickers/color-pickers.module#ColorPickersModule'},
  {path: '', redirectTo: 'angular-editor-fabric'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AngularNpmsRoutingModule {
}
