import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AngularEditorFabricComponent} from '../../../../../../shared/components/npms/angular-editor-fabric/angular-editor-fabric.component';

const routes: Routes = [
  {path: '', component: AngularEditorFabricComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AngularEditorFabricRoutingModule {
}
