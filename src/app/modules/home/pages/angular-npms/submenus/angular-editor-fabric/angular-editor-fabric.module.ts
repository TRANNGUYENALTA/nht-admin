import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';
import { AngularEditorFabricRoutingModule } from './angular-editor-fabric-routing.module';
import { AngularEditorFabricComponent } from '@shared/components/npms/angular-editor-fabric/angular-editor-fabric.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [AngularEditorFabricComponent],
  imports: [
    CommonModule,
    FormsModule,
    ColorPickerModule,
    AngularEditorFabricRoutingModule
  ]
})
export class AngularEditorFabricModule { }
