import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ColorPickerComponent} from '../../../../../../shared/components/npms/color-picker/color-picker.component';

const routes: Routes = [
  {path: '', component: ColorPickerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColorPickersRoutingModule { }
