import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';

import { ColorPickersRoutingModule } from './color-pickers-routing.module';
import { ColorPickerComponent } from '@shared/components/npms/color-picker/color-picker.component';
import {SharedModule} from '@shared/shared.module';

@NgModule({
  declarations: [ColorPickerComponent],
  imports: [
    CommonModule,
    ColorPickerModule,
    SharedModule,
    ColorPickersRoutingModule
  ]
})
export class ColorPickersModule { }
