import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'form-layouts', loadChildren: './submenus/form-layout/form-layout.module#FormLayoutModule'},
  {path: 'form-advanced', loadChildren: './submenus/form-advanced/form-advanced.module#FormAdvancedModule'},
  {path: 'form-wizard', loadChildren: './submenus/form-wizard/form-wizard.module#FormCustomWizardModule'},
  {path: '', redirectTo: 'form-layouts'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
