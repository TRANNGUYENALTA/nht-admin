import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FormLayoutRoutingModule} from './form-layout-routing.module';
import {FormLayoutsComponent} from './form-layouts.component';
import {
  VerticalFormComponent,
  RoundVerticalFormComponent,
  HorizontalFormComponent,
  RoundHorizontalFormComponent
} from '../../../../../../shared/components';

@NgModule({
  declarations: [
    FormLayoutsComponent,
    VerticalFormComponent,
    RoundVerticalFormComponent,
    HorizontalFormComponent,
    RoundHorizontalFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    FormLayoutRoutingModule
  ]
})
export class FormLayoutModule {
}
