import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormLayoutsComponent} from './form-layouts.component';
import {
  VerticalFormComponent,
  RoundVerticalFormComponent,
  HorizontalFormComponent,
  RoundHorizontalFormComponent
} from '../../../../../../shared/components';

const routes: Routes = [
  {path: '', component: FormLayoutsComponent},
  {path: 'vertical-form', component: VerticalFormComponent},
  {path: 'round-vertical-form', component: RoundVerticalFormComponent},
  {path: 'horizontal-form', component: HorizontalFormComponent},
  {path: 'round-horizontal-form', component: RoundHorizontalFormComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormLayoutRoutingModule {
}
