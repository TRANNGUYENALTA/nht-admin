import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicFormWizardComponent } from './basic-form-wizard.component';

describe('BasicFormWizardComponent', () => {
  let component: BasicFormWizardComponent;
  let fixture: ComponentFixture<BasicFormWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicFormWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicFormWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
