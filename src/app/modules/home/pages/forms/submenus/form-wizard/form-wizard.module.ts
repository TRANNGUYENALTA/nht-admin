import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {FormWizardModule} from 'angular2-wizard/dist';
import { FormWizardRoutingModule } from './form-wizard-routing.module';
import { FormWizardComponent } from './form-wizard.component';
import { BasicFormWizardComponent } from './components/basic-form-wizard/basic-form-wizard.component';

@NgModule({
  declarations: [FormWizardComponent, BasicFormWizardComponent],
  imports: [
    CommonModule,
    FormsModule,
    FormWizardModule,
    FormWizardRoutingModule
  ]
})
export class FormCustomWizardModule { }
