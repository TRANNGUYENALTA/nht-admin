export class FwizardInfo {
  public username: string;
  public password: string;
  public repassword: string;
  public firstname: string;
  public lastname: string;
  public email: string;
  public address: string;
  public confirm: boolean;

  constructor() {
    this.username = '';
    this.password = '';
    this.firstname = '';
    this.lastname = '';
    this.email = '';
    this.address = '';
    this.confirm = false;
  }
}
