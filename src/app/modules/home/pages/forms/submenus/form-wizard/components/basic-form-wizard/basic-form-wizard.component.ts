import {Component, OnInit} from '@angular/core';
import {init} from './init-basic-formwizard.';
import {FwizardInfo} from './fwizard-info';

declare var $: any;

@Component({
  selector: 'app-basic-form-wizard',
  templateUrl: './basic-form-wizard.component.html',
  styleUrls: ['./basic-form-wizard.component.scss']
})
export class BasicFormWizardComponent implements OnInit {
  public infor = new FwizardInfo();

  constructor() {
  }

  ngOnInit() {
    init();
  }

  showInfor(f) {
    console.log($('#userNameCoppy').val());
    console.log(f);
  }

  onStep1Next(event) {
    console.log(this.infor);
  }

  onStep2Next(event) {
  }

  onStep3Next(event) {
  }

  onComplete(event) {
    alert(1);
  }

}
