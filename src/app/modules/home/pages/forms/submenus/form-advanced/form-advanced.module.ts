import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {FormAdvancedRoutingModule} from './form-advanced-routing.module';
import {FormAdvancedComponent} from './form-advanced.component';
import {
  DatePickerComponent,
  InlineDatepickerComponent,
  SelectInputsComponent,
  InputTagsComponent
} from '../../../../../../shared/components';

@NgModule({
  declarations: [
    FormAdvancedComponent,
    DatePickerComponent,
    InlineDatepickerComponent,
    SelectInputsComponent,
    InputTagsComponent],
  imports: [
    CommonModule,
    FormsModule,
    FormAdvancedRoutingModule
  ]
})
export class FormAdvancedModule {
}
