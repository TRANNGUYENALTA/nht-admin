import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormAdvancedComponent} from './form-advanced.component';
import {
  DatePickerComponent,
  InlineDatepickerComponent,
  SelectInputsComponent,
  InputTagsComponent
} from '../../../../../../shared/components';

const routes: Routes = [
  {path: '', component: FormAdvancedComponent},
  {path: 'date-picker', component: DatePickerComponent},
  {path: 'inline-datepicker', component: InlineDatepickerComponent},
  {path: 'select-input', component: SelectInputsComponent},
  {path: 'input-tags', component: InputTagsComponent},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormAdvancedRoutingModule {
}
