import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GoogleMapsRoutingModule} from './google-maps-routing.module';
import {GoogleMapsComponent} from './google-maps.component';
import {AgmCoreModule} from '@agm/core';
import {BasicGoogleMapsComponent} from '../../../../../../shared/components/maps/basic-google-maps/basic-google-maps.component';
import {MapMarkerComponent} from '../../../../../../shared/components/maps/map-marker/map-marker.component';
import {OverLayerMapComponent} from '../../../../../../shared/components/maps/over-layer-map/over-layer-map.component';
import {PolygonalMapComponent} from '../../../../../../shared/components/maps/polygonal-map/polygonal-map.component';
import {StyledMapCustomComponent} from '../../../../../../shared/components/maps/styled-map-custom/styled-map-custom.component';

@NgModule({
  declarations: [
    GoogleMapsComponent,
    BasicGoogleMapsComponent,
    MapMarkerComponent,
    OverLayerMapComponent,
    PolygonalMapComponent,
    StyledMapCustomComponent],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDKXKdHQdtqgPVl2HI2RnUa_1bjCxRCQo4'
    }),
    GoogleMapsRoutingModule
  ]
})
export class GoogleMapsModule {
}
