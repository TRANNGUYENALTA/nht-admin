import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: 'google-maps', loadChildren: './submenus/google-maps/google-maps.module#GoogleMapsModule'},
  {path: '**', redirectTo: 'google-maps'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapsRoutingModule {
}
