import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: 'chartjs', loadChildren: './submenus/chartjs/chartjs.module#ChartjsModule'},
  {path: 'morris-chart', loadChildren: './submenus/morris-chart/morris-chart.module#MorrisChartModule'},
  {path: 'other-chart', loadChildren: './submenus/other-chart/other-chart.module#OtherChartModule'},
  {path: '', redirectTo: 'chartjs'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartsRoutingModule {
}
