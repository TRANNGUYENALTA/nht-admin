import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MorrisChartsComponent} from './morris-charts.component';
import {
  AreaMorrisChartComponent,
  BarMorrisChartComponent,
  DonutMorrisChartComponent,
  LineMorrisChartComponent
} from '../../../../../../shared/components';

const routes: Routes = [
  {path: '', component: MorrisChartsComponent},
  {path: 'bar-chart', component: BarMorrisChartComponent},
  {path: 'donut-chart', component: DonutMorrisChartComponent},
  {path: 'line-chart', component: LineMorrisChartComponent},
  {path: 'area-chart', component: AreaMorrisChartComponent},
  {path: '**', redirectTo: ''},
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MorrisChartRoutingModule { }
