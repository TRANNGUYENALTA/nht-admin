import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MorrisChartRoutingModule} from './morris-chart-routing.module';
import {MorrisChartsComponent} from './morris-charts.component';
import {
  BarMorrisChartComponent,
  DonutMorrisChartComponent,
  LineMorrisChartComponent,
  AreaMorrisChartComponent
} from '../../../../../../shared/components';


@NgModule({
  declarations: [
    MorrisChartsComponent,
    BarMorrisChartComponent,
    DonutMorrisChartComponent,
    LineMorrisChartComponent,
    AreaMorrisChartComponent],
  imports: [
    CommonModule,
    MorrisChartRoutingModule
  ]
})
export class MorrisChartModule {
}
