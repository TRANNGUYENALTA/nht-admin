import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OtherChartRoutingModule} from './other-chart-routing.module';
import {OtherChartComponent} from './other-chart.component';
import {EasyPieChartComponent} from '../../../../../../shared/components';
import {JqueryKnobChartComponent} from '../../../../../../shared/components/jquery-knob-chart/jquery-knob-chart.component';
import { EasyPieChartsComponent } from '../../../../../../shared/components/easy-pie-charts/easy-pie-charts.component';

@NgModule({
  declarations: [OtherChartComponent, EasyPieChartComponent, JqueryKnobChartComponent, EasyPieChartsComponent],
  imports: [
    CommonModule,
    OtherChartRoutingModule
  ]
})
export class OtherChartModule {
}
