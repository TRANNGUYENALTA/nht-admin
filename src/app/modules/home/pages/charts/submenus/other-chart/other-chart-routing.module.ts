import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OtherChartComponent} from './other-chart.component';

const routes: Routes = [
  {path: '', component: OtherChartComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherChartRoutingModule {
}
