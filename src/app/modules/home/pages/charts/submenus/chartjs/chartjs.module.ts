import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartjsRoutingModule } from './chartjs-routing.module';
import { CharjsComponent } from './charjs.component';
import {SharedModule} from '../../../../../../shared/shared.module';

@NgModule({
  declarations: [CharjsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ChartjsRoutingModule,
  ]
})
export class ChartjsModule { }
