import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CharjsComponent} from './charjs.component';
import {
  LineChartComponent,
  BarchartComponent,
  PolarComponent,
  PiechartComponent,
  DoughnutChartComponent} from '../../../../../../shared/components';

const routes: Routes = [
  {path: '', component: CharjsComponent},
  {path: 'line-chart', component: LineChartComponent},
  {path: 'bar-chart', component: BarchartComponent},
  {path: 'polar-chart', component: PolarComponent},
  {path: 'pie-chart', component: PiechartComponent},
  {path: 'doughnut-chart', component: DoughnutChartComponent},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartjsRoutingModule { }
