import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-charjs',
  templateUrl: './charjs.component.html',
  styleUrls: ['./charjs.component.css']
})
export class CharjsComponent implements OnInit {
  // data line chart js
  labelLineChart = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
  firstData = {
    'title': 'Google',
    'data': [13, 20, 4, 18, 7, 4, 8]
  };
  secondData = {
    'title': 'Facebook',
    'data': [3, 30, 6, 6, 3, 4, 11]
  };
  // data bar chart
  labelBarChart = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
  firstDataBarChart = {
    'title': 'Google',
    'data': [13, 20, 4, 18, 29, 25, 8]
  };
  secondDataBarChart = {
    'title': 'Facebook',
    'data': [31, 30, 6, 6, 21, 4, 11]
  };

  // data polar chart
  dataPolarChart = [13, 20, 11, 40];
  labelPolarChart = ['Zalo', 'Twitter', 'Printest', 'FaceBook'];

// data polar chart
  dataPieChart = [13, 120, 11, 20];
  labelPieChart = ['Zalos', 'Twitters', 'Printests', 'FaceBooks'];

// data Doughnut chart
  dataDoughnutChart = [13, 120, 11, 20];
  labelDoughnutChart = ['Zalos', 'Twitters', 'Printests', 'FaceBooks'];

  constructor() {
  }

  ngOnInit() {
  }

}
