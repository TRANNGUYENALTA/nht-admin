import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'ecommerce', loadChildren: './submenus/ecommerce/ecommerce.module#EcommerceModule'},
  {path: 'show-component', loadChildren: './submenus/show-component/show-component.module#ShowComponentModule'},
  {path: '', redirectTo: 'ecommerce'}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
