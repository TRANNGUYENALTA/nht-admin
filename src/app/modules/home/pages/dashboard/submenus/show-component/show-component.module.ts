import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowComponentRoutingModule } from './show-component-routing.module';
import { ShowComponentComponent } from './show-component.component';
import {SharedModule} from '../../../../../../shared/shared.module';


@NgModule({
  declarations: [ShowComponentComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShowComponentRoutingModule
  ]
})
export class ShowComponentModule { }
