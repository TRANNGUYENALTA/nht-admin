import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShowComponentComponent} from './show-component.component';

const routes: Routes = [
  {
  path: '', component: ShowComponentComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowComponentRoutingModule { }
