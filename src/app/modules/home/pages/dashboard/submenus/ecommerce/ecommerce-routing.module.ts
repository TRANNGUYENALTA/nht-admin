import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EcommerceComponent} from './ecommerce.component';
import {
  SiteTraficComponent,
  TotalCounterComponent,
  WeekysaleComponent,
  WorldSellingRegionComponent,
  PageViewsComponent,
  TotalClicksComponent,
  TotalDownloadComponent,
  DeviceStorageComponent,
  TotalEarningSalesComponent,
  CustomerReviewComponent,
  RecentOrderTableComponent
} from '../../../../../../shared/components';


const routes: Routes = [
  {path: '', component: EcommerceComponent},
  {path: 'total-statistical', component: TotalCounterComponent},
  {path: 'weeky-sale', component: WeekysaleComponent},
  {path: 'site-traffic', component: SiteTraficComponent},
  {path: 'world-selling-region', component: WorldSellingRegionComponent},
  {path: 'page-views', component: PageViewsComponent},
  {path: 'total-click', component: TotalClicksComponent},
  {path: 'total-downloads', component: TotalDownloadComponent},
  {path: 'device-storage', component: DeviceStorageComponent},
  {path: 'total-earning-sale', component: TotalEarningSalesComponent},
  {path: 'customer-review', component: CustomerReviewComponent},
  {path: 'recent-order-tables', component: RecentOrderTableComponent},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EcommerceRoutingModule {
}
