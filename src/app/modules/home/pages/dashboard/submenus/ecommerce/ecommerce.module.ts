import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EcommerceRoutingModule} from './ecommerce-routing.module';
import {TotalCounterComponent} from '../../../../../../shared/components/counters/total-counter/total-counter.component';
import {EcommerceComponent} from './ecommerce.component';
import {SharedModule} from '../../../../../../shared/shared.module';
import {
  SiteTraficComponent,
  StatisticalCounterComponent,
  OneSocialStatisticalComponent,
  SocialStatisticalComponent,
  WorldSellingRegionComponent
} from '../../../../../../shared/components/index';
import { PageViewsComponent } from '../../../../../../shared/components/page-views/page-views.component';
import { TotalClicksComponent } from '../../../../../../shared/components/total-clicks/total-clicks.component';
import { TotalDownloadComponent } from '../../../../../../shared/components/total-download/total-download.component';
import { DeviceStorageComponent } from '../../../../../../shared/components/device-storage/device-storage.component';
import { RecentOrderTableComponent } from '../../../../../../shared/components/recent-order-table/recent-order-table.component';
import { TotalEarningSalesComponent } from '../../../../../../shared/components/total-earning-sales/total-earning-sales.component';
import { CustomerReviewComponent } from '../../../../../../shared/components/customer-review/customer-review.component';

const COMPONENTS = [
  TotalCounterComponent,
  EcommerceComponent,
  StatisticalCounterComponent,
  SiteTraficComponent
];

@NgModule({
  declarations: [...COMPONENTS, SocialStatisticalComponent, OneSocialStatisticalComponent, WorldSellingRegionComponent, PageViewsComponent, TotalClicksComponent, TotalDownloadComponent, DeviceStorageComponent, RecentOrderTableComponent, TotalEarningSalesComponent, CustomerReviewComponent],
  imports: [
    CommonModule,
    SharedModule,
    EcommerceRoutingModule
  ]
})
export class EcommerceModule {
}
