import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SmartTableRoutingModule } from './smart-table-routing.module';
import {SharedModule} from '@shared/shared.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    SmartTableRoutingModule
  ]
})
export class SmartTableModule { }
