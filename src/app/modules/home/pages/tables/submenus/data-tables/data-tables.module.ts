import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTablesRoutingModule } from './data-tables-routing.module';
import { DataTablesComponent } from './data-tables.component';
import { DataTableExportComponent, DataTableExampleComponent } from '@shared/components';

@NgModule({
  declarations: [DataTableExampleComponent, DataTablesComponent, DataTableExportComponent],
  imports: [
    CommonModule,
    DataTablesRoutingModule
  ]
})
export class DataTablesModule { }
