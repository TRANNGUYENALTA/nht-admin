import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DataTablesComponent} from './data-tables.component';
import {DataTableExampleComponent} from '../../../../../../shared/components/tables/data-table-example/data-table-example.component';
import {DataTableExportComponent} from '../../../../../../shared/components/tables/data-table-export/data-table-export.component';

const routes: Routes = [
  {path: '', component: DataTablesComponent},
  {path: 'data-table-example', component: DataTableExampleComponent},
  {path: 'data-table-export', component: DataTableExportComponent},
  {path: '**', redirectTo: ''}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataTablesRoutingModule {
}
