import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SimpleTablesRoutingModule } from './simple-tables-routing.module';
import { SimpleTablesComponent } from './simple-tables.component';
import { BasicTableComponent } from '@shared/components';

@NgModule({
  declarations: [SimpleTablesComponent, BasicTableComponent],
  imports: [
    CommonModule,
    SimpleTablesRoutingModule
  ]
})
export class SimpleTablesModule { }
