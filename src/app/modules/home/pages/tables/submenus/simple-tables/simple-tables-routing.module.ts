import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SimpleTablesComponent} from './simple-tables.component';
import {BasicTableComponent} from '../../../../../../shared/components/tables/basic-table/basic-table.component';

const routes: Routes = [
  {path: '', component: SimpleTablesComponent},
  {path: 'basic-table', component: BasicTableComponent},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimpleTablesRoutingModule {
}
