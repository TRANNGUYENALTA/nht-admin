import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../../shared/shared.module';
import { TablesRoutingModule } from './tables-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    TablesRoutingModule
  ]
})
export class TablesModule { }
