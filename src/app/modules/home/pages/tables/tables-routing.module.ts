import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'data-table', loadChildren: './submenus/data-tables/data-tables.module#DataTablesModule'},
  {path: 'simple-table', loadChildren: './submenus/simple-tables/simple-tables.module#SimpleTablesModule'},
  {path: 'smart-table', loadChildren: './submenus/smart-table/smart-table.module#SmartTableModule'},
  {path: '**', redirectTo: 'data-table'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TablesRoutingModule { }
