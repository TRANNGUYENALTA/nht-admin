import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarsRoutingModule } from './calendars-routing.module';
import { CalendarsComponent } from './calendars.component';
import {FullCalendarComponent} from '../../../../shared/components';
import {SharedModule} from '../../../../shared/shared.module';

@NgModule({
  declarations: [CalendarsComponent, FullCalendarComponent],
  imports: [
    CommonModule,
    SharedModule,
    CalendarsRoutingModule
  ]
})
export class CalendarsModule { }
