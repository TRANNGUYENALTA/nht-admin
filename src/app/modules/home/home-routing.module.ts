import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {AuthGuard} from '../../cores/guards/auth.guard';

const routes: Routes = [
  {
    path: '', component: HomeComponent, canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardModule'},
      {path: 'charts', loadChildren: './pages/charts/charts.module#ChartsModule'},
      {path: 'calendars', loadChildren: './pages/calendars/calendars.module#CalendarsModule'},
      {path: 'components', loadChildren: './pages/components/components.module#ComponentsModule'},
      {path: 'forms', loadChildren: './pages/forms/forms.module#FormModule'},
      {path: 'tables', loadChildren: './pages/tables/tables.module#TablesModule'},
      {path: 'maps', loadChildren: './pages/maps/maps.module#MapsModule'},
      {path: 'angular-npms', loadChildren: './pages/angular-npms/angular-npms.module#AngularNpmsModule'},
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: '**', redirectTo: 'login'},
    ]
  },
  {path: '**', redirectTo: 'login'}

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
