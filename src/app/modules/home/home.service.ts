import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SharedApiService} from '../../shared/share_service/shared-api.service';

@Injectable()
export class HomeService {
  // create new eventEmitter
  newEventEmitter = new EventEmitter<string>();

  constructor(private shareApi: SharedApiService) {
  }

  getList(): Observable<any> {
    return this.shareApi.getApi('/api/employee/show-list');
  }
}
